import AsyncStorage from "@react-native-async-storage/async-storage";

export const OdooConfig = async () => {
    const config = {
      host: (await AsyncStorage.getItem('host')) ?? '192.168.1.50',
      port: (await AsyncStorage.getItem('port')) ?? '8071',
      database: (await AsyncStorage.getItem('database')) ?? 'dpi_app'
    };
    return config;
  };
  
  export const version = '1.5';

/*   host: (await AsyncStorage.getItem('host')) ?? '192.168.1.19',
  port: (await AsyncStorage.getItem('port')) ?? '8171',
  database: (await AsyncStorage.getItem('database')) ?? 'dpi20220818' */
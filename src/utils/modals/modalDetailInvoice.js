import React from 'react';
import { View, Text, FlatList } from "react-native";
import { Button, Icon } from 'react-native-elements';
import { ItemInvoice } from '../../components/invoices/components/ItemInvoice';

export const ModalDetailInvoice = (props) => {
    const { details, closeModal } = props
    return (
        <View style={{ backgroundColor: 'white', borderRadius: 10, borderWidth: 1, borderColor: 'gray' }}>
            <View style={{ width: '100%', flexDirection: 'row' }}>
                <View style={{ width: '20%' }}></View>
                <View style={{ width: '60%', justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 18 }}>Detalles de la factura</Text>
                </View>
                <View style={{ width: '20%' }}>
                    <Button
                        containerStyle={{ justifyContent: 'center' }}
                        onPress={() => closeModal()}

                        type="clear" icon={<Icon name="close" color='black' />}>

                    </Button>
                </View>

            </View>
            <View style={{
                flexDirection: 'row', justifyContent: 'space-between',
                backgroundColor: '#E0E0E0', padding: 5,
            }}>
                <View style={{ borderRightWidth: 1, width: '25%', }}>
                    <Text style={{ fontSize: 14, textAlign: 'center' }}>Producto</Text>
                </View>
                <View style={{ borderRightWidth: 1, width: '30%', }}>
                    <Text style={{ fontSize: 14, textAlign: 'center' }}>Cantidad</Text>
                </View>
                <View style={{ borderRightWidth: 1, width: '25%', }}>
                    <Text style={{ fontSize: 14, textAlign: 'center' }}>Precio</Text>
                </View>
                <View style={{ width: '10%', borderRightWidth: 1 }}>
                    <Icon name="flag" type="font-awesome-5" color='black' size={15} />
                </View>
                <View style={{ width: '10%', }}>

                </View>
            </View>
            <View style={{ height: 150 }}>
                <FlatList
                    data={details}
                    renderItem={({ item }) =>
                        <ItemInvoice  item={item}  />
                    }
                    keyExtractor={item => "key" + item.id}

                //ListFooterComponent={<View style={{ height: 100 }} />} 
                />
            </View>
        </View>
    )
}
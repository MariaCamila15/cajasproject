import   React,{useState} from 'react';
import {View, TouchableOpacity,ToastAndroid, PermissionsAndroid, Text,StyleSheet,Image, Alert} from 'react-native'
import { Input,Button, Icon } from 'react-native-elements'
import { useFormik } from 'formik'
import { UploadTransaction } from '../../services/box.services';
import { FormatDateComplete } from '../inputs/formats';
import { launchCamera } from 'react-native-image-picker';
import { useEffect } from 'react';

 const ModalRegisterPayment = (props) => {
    const{invoice_id, closeModal} =  props
    let [image, setImage] = useState('');
    let [filePath, setFilePath] = useState('');


    const callback = (flag,response) =>{
        if(flag){
            if(response.errores){
                Alert.alert("Atención",response.errores.mensaje)
                
            }else{
                ToastAndroid.show("Pago registrado correctamente",ToastAndroid.SHORT)

            }
           
        }else{
            ToastAndroid.show("Estamos teniendo problemas en el servidor, inténtalo más tarde",ToastAndroid.LONG)
        }
        closeModal()

    }

    const takePicture = async (type) =>{
        let options = {
            mediaType:type,
            quality: 1,
            includeBase64: true,   
        };
        let isCameraPermitted = await requestCameraPermission();
        let isStoragePermitted = await requestExternalWritePermission();
        if (isCameraPermitted && isStoragePermitted) {
            launchCamera(options, (response) => {
                if (response.didCancel) {
                    ToastAndroid.show("El usuario ha cancelado tomar la foto",ToastAndroid.SHORT)
                    return;
                } else if (response.errorCode == 'camera_unavailable') {
                    ToastAndroid.show("Cámara no disponible en dispositivo",ToastAndroid.SHORT)
                    return;
                } else if (response.errorCode == 'permission') {
                    ToastAndroid.show("Permiso de tomar foto denegado",ToastAndroid.SHORT)
                    return;
                } else if (response.errorCode == 'others') {
                    ToastAndroid.show(response.errorMessage,ToastAndroid.SHORT)
                    return;
                }
                setImage(response.assets[0].uri)
                setFilePath(response.assets[0].base64);
            });
        }
    }
    const requestCameraPermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission',
                    },
                );
                    // If CAMERA Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                return false;
            }
        } else return true;
    };
    const requestExternalWritePermission = async () => {
        if (Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'External Storage Write Permission',
                        message: 'App needs write permission',
                    },
                );
                // If WRITE_EXTERNAL_STORAGE Permission is granted
                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.warn(err);
                alert('Write permission err', err);
            }
            return false;
        } else return true;
    };

    const {values,isSubmitting, setFieldValue, handleSubmit, errors, resetForm} = useFormik({
        initialValues:{
        amount:'',
        },
        onSubmit: values=>{  
          resetForm();
          ToastAndroid.show("Enviando info", ToastAndroid.SHORT);   
          const data = [
            {
                id: -1,
                detalle: {
                    latitud: "-0.2988",
                    longitud: "-0.23",
                    tipo: 'pago',
                    gasto_foto_ids: [
                        [
                            0, 0, {
                                foto: filePath 
                            }
                        ]
                    ],
                    hora_dispositivo: FormatDateComplete(Date.now()),
                    valor: parseInt(values.amount),
                    pago_factura_venta_id: parseInt(invoice_id),
                    //venta_partner_id: false,
                }
            }
        ]
        UploadTransaction(data,callback)
        },
        validate: values =>{
            const errors={}
            if(values.amount.length === 0) errors.amount="Debe ingresar un monto"
            return errors
        }
    })

    return (   
        <>
                 <View style = {style.caja}>    
                    <View style = {style.formulario}>
                        <Input style = {style.textoFormularioView}
                            value={values.amount}
                            onChangeText={text => setFieldValue('amount',text)}
                            errorMessage={errors.amount}
                            placeholder='Monto'
                            keyboardType='numeric'
                            placeholderTextColor={'gray'}
                        />
                        </View> 
                        <View style={{ width: '90%', borderWidth: 2, borderColor: '#023047', alignSelf: 'center', marginTop: 10 }}>
                            {<Button type="clear" onPress={()=>{takePicture('photo')}} icon={<Icon name="camera-alt" color='gray'></Icon>}>ds</Button>}
                             {image != '' && <Image style={{ height: 200, width: "100%", alignSelf: "center" }} source={{ uri:image }} />} 
                        </View>      
                        <TouchableOpacity style = {style.touchable} onPress={handleSubmit}>
                            <Text> Registrar pago </Text>
                        </TouchableOpacity> 
                </View> 
        </>
    ); 
}

 const style = StyleSheet.create({
    container: {
        
         height: '100%',
         alignItems: 'center',
         width:'100%',
         resizeMode:'stretch'
    },
    
    logo:{
        marginTop:10,
        height:'10%',
        width:'19%'
    },
    loading:{
        position: 'absolute',
        marginLeft:50,
        marginTop: 100,

    },
    bienvenido:{
        height:'10%',
        width:'60%'
    },
    caja:{
        alignSelf: 'center',
        backgroundColor: 'white',
        borderRadius: 12,
        height: '66%',
        width: '90%'
    },
    
    usuario:{
        alignItems: 'center',
        height: '25%',
        width: '98%',
        marginTop:7
    },
    logoUsuario:{
        height: '85%',
        width: '34%',
        marginTop:5
    },

    formulario:{
        alignSelf:'center',
        height: '13%',
        width: '70%',
        flexDirection:'row',
        marginTop:15
    },

    logoFormularioView:{
        marginTop:20,
        height: 15,
        width: 15,
        marginLeft: 15

    },
    textoFormularioView:{
        height: '15%',
        width: 6,
        borderBottomWidth : 0
    },
    
    textoContraseña:{
        marginTop:10,
        marginLeft:10
    },
    touchable:{
        alignSelf:'center',
        marginTop:25,
        height:'10%',
        width:'60%',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: 'rgba(0,0,0,0.2)',
        fontFamily:''     
    }
}); 

export default ModalRegisterPayment
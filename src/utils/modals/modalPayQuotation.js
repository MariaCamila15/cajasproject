import   React,{useState} from 'react';
import {View, TouchableOpacity,ToastAndroid, Text,StyleSheet, Alert} from 'react-native'
import { Input } from 'react-native-elements'
import { useFormik } from 'formik'
import { CreateCreditNote } from '../../services/services';

 const ModalPayQuotation = (props) => {
    const{quotation_id, closeModal} =  props
    const callback = (flag,response) =>{
        if(flag){
            ToastAndroid.show("Factura creada Correctamente", ToastAndroid.SHORT);   
        }else{
            Alert.alert("¡Atención!",response.data.arguments[0])
        }
        closeModal()

    }
    const {values,isSubmitting, setFieldValue, handleSubmit, errors, resetForm} = useFormik({
        initialValues:{
        amount:'',
        },
        onSubmit: values=>{  
          resetForm();
          ToastAndroid.show("Enviando info", ToastAndroid.SHORT);   
          const data = {
            cotizacion: parseInt(quotation_id),
            amount: values.amount        
        }
        CreateCreditNote(data,callback);
        
        
        },
        validate: values =>{
          const errors={}
          if(values.amount.length === 0) errors.amount="Debe ingresar un monto"
          return errors
        }   
    })
    return (   
        <>
                 <View style = {style.caja}>    
                    <View style = {style.formulario}>
                        <Input style = {style.textoFormularioView}
                            value={values.amount}
                            onChangeText={text => setFieldValue('amount',text)}
                            errorMessage={errors.amount}
                            placeholder='Monto'
                            keyboardType='numeric'
                            placeholderTextColor={'gray'}
                        />
                        </View>       
                        <TouchableOpacity style = {style.touchable} onPress={handleSubmit}>
                            <Text> Abonar </Text>
                        </TouchableOpacity> 
                </View> 
        </>
    ); 
}

 const style = StyleSheet.create({
    container: {
        
         height: '100%',
         alignItems: 'center',
         width:'100%',
         resizeMode:'stretch'
    },
    
    logo:{
        marginTop:10,
        height:'10%',
        width:'19%'
    },
    loading:{
        position: 'absolute',
        marginLeft:50,
        marginTop: 100,

    },
    bienvenido:{
        height:'10%',
        width:'60%'
    },
    caja:{
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 12,
        height: '66%',
        width: '80%'
    },
    
    usuario:{
        alignItems: 'center',
        height: '25%',
        width: '98%',
        marginTop:7
    },
    logoUsuario:{
        height: '85%',
        width: '34%',
        marginTop:5
    },

    formulario:{
        height: '13%',
        width: '70%',
        flexDirection:'row',
        marginTop:15
    },

    logoFormularioView:{
        marginTop:20,
        height: 15,
        width: 15,
        marginLeft: 15

    },
    textoFormularioView:{
        height: '15%',
        width: 6,
        borderBottomWidth : 0
    },
    
    textoContraseña:{
        marginTop:10,
        marginLeft:10
    },
    touchable:{
        marginTop:25,
        height:'10%',
        width:'60%',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: 'rgba(0,0,0,0.2)',
        fontFamily:''     
    }
}); 

export default ModalPayQuotation
import   React,{useState, useEffect} from 'react';
import {View, TouchableOpacity, Text,StyleSheet, ToastAndroid} from 'react-native'
import { useFormik } from 'formik'
import {Picker} from '@react-native-picker/picker';
import {EditLead} from '../../services/services';

 const ModalEditLead = (props) => {
    const{ closeModal, lead_id} =  props
    const [stages, setStages] = useState([])
    const [users, setUsers] = useState([{label: "Lina Paola González Jiménez", value:7},
                                        {label: "Jimena Sanchez Vásquez", value:8},
                                        {label: "Martin Gomez", value:10},
                                        {label: "Sebastián Ramirez Holguín", value:9}])
                                    
    const [user_id,setUser_id] = useState('')
    const [stage_id,setStage_id] = useState('')
    const didCancel = () =>{
        closeModal()

    }
    const callback = (flag,response) =>{
        if(flag){
            if(response){
                ToastAndroid.show("Oportunidad actualizada correctamente", ToastAndroid.SHORT)
                closeModal()
            }
        }else{
            ToastAndroid.show("Estamos teniendo problemas con el servidor", ToastAndroid.SHORT)
                closeModal() 

        }

    }
    const {values,isSubmitting, setFieldValue, handleSubmit, errors, resetForm} = useFormik({
        initialValues:{
        description:'',
        },
        onSubmit: values=>{  
            resetForm();
            ToastAndroid.show("Enviando...",ToastAndroid.SHORT);
            let data = {
                stage_id: parseInt(stage_id),
                id:  parseInt(lead_id),
                user_id:  parseInt(user_id) //8
            }
            EditLead(data,callback)
            
            
        },  
    })
    useEffect(() => {
        let stages = props.stages
        setStages(stages)
        let stage= props.stage_id
        setStage_id(stage.toString())
       }, [])  
    return (   
        <>
                 <View style = {style.caja}>    
                    <View style = {style.formulario}>
                        <Text style={{alignSelf:'center', fontWeight:'bold', fontSize:20}}>Estado</Text>
                            
                        <Picker
                            containerStyle={{
                            backgroundColor: 'white',
                            marginHorizontal: 10,
                            borderColor: 'black',
                            borderRadius: 10,
                            }}
                            selectedValue={stage_id}
                            style={{ height: 60, }}
                            onValueChange={(itemValue, itemIndex) => {
                            setStage_id(itemValue)
                            }
                        }>
                        {
                          stages.map((element, index) => {
                          return <Picker.Item key={'key' + element.value} label={element.label} value={element.value} />
                          })
                          }
                        </Picker>
                        <Text style={{alignSelf:'center', fontWeight:'bold', fontSize:20}}>Usuario Asignado</Text>
                        <Picker
                            containerStyle={{
                            backgroundColor: 'white',
                            marginHorizontal: 10,
                            borderColor: 'black',
                            borderRadius: 10,
                            }}
                            selectedValue={user_id}
                            style={{ height: 60, }}
                            onValueChange={(itemValue, itemIndex) => {
                            setUser_id(itemValue)
                            }
                        }>
                        {
                          users.map((element, index) => {
                          return <Picker.Item key={'key' + element.value} label={element.label} value={element.value} />
                          })
                          }
                        </Picker>
                    </View >  
                    <View style={{height: '90%', marginLeft:10, flexDirection:'row'}}>     
                        <TouchableOpacity style = {style.save} onPress={handleSubmit}>
                            <Text style={{color:'white'}}> Modificar Pago</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style = {style.cancel} onPress={didCancel}>
                            <Text style={{color:'white'}}> Cancelar </Text>
                        </TouchableOpacity>
                    </View>
                </View> 
        </>
    ); 
}

 const style = StyleSheet.create({
    container: {
         height: '100%',
         alignItems: 'center',
         width:'100%',
         resizeMode:'stretch'
    },
    
    
    caja:{
        alignSelf:'center',
        backgroundColor: 'white',
        borderRadius: 12,
        height: 400,
        width: '90%'
    },
    
    usuario:{
        alignItems: 'center',
        height: '25%',
        width: '98%',
        marginTop:7
    },
    logoUsuario:{
        height: '85%',
        width: '34%',
        marginTop:5
    },

    formulario:{
        height: '50%',
        width: '100%',
        flexDirection:'column',
        marginTop:15
    },

    logoFormularioView:{
        marginTop:20,
        height: 15,
        width: 15,
        marginLeft: 15

    },
    textoFormularioView:{
        height: '15%',
        width: 6,
        borderBottomWidth : 0
    },
    
    textoContraseña:{
        marginTop:10,
        marginLeft:10
    },
    save:{
        marginTop:15,
        height:'10%',
        width:'40%',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: '#219EBC',
        marginLeft:15     
    },
    cancel:{
        marginTop:15,
        height:'10%',
        width:'40%',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: '#023047',
        marginLeft:15     
    }
}); 

export default ModalEditLead
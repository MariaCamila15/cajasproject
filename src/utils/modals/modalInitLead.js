import   React,{useState} from 'react';
import {View, TouchableOpacity,ToastAndroid, Text,StyleSheet} from 'react-native'
import { Input } from 'react-native-elements'
import { useFormik } from 'formik'
import { RecordTime} from '../../services/services';

 const ModalInitLead = (props) => {
    const{lead_id, project_id, closeModal} =  props
    const {values,isSubmitting, setFieldValue, handleSubmit, errors, resetForm} = useFormik({
        initialValues:{
        description:'',
        },
        onSubmit: values=>{  
          resetForm();
          ToastAndroid.show("Enviando info", ToastAndroid.SHORT);   
          const data = {
            name:values.description,
            project_id: project_id,
            lead_id: parseInt(lead_id)
        }
        RecordTime(data,(flag,response)=>{
            if (flag){
                ToastAndroid.show("Tiempo de tarea registrado", ToastAndroid.SHORT);
                closeModal();
               
            }
        })
        },
        validate: values =>{
          const errors={}
          if(values.description.length === 0) errors.description="Debe ingresar una descripcion"
          return errors
        }   
    })
    return (   
        <>
        <View style = {style.caja}>
            <View style = {style.formulario}>
                <Input style = {style.textoFormularioView}
                    value={values.description}
                    onChangeText={text => setFieldValue('description',text)}
                    errorMessage={errors.description}
                    placeholder='Descripción'
                    placeholderTextColor={'black'}
                />
            </View>
            <TouchableOpacity style = {style.touchable} onPress={handleSubmit}>
                <Text> Registrar Tiempo </Text>
            </TouchableOpacity> 
        </View> 
        </>
    ); 
}

 const style = StyleSheet.create({
    container: {
        
         height: '100%',
         alignItems: 'center',
         width:'100%',
         resizeMode:'stretch'
    },
    
    logo:{
        marginTop:10,
        height:'10%',
        width:'19%'
    },
    loading:{
        position: 'absolute',
        marginLeft:50,
        marginTop: 100,

    },
    bienvenido:{
        height:'10%',
        width:'60%'
    },
    caja:{
        alignItems: 'center',
        alignSelf:'center',
        backgroundColor: 'white',
        borderRadius: 12,
        height: 200,
        width: '80%'
    },
    
    usuario:{
        alignItems: 'center',
        height: '25%',
        width: '98%',
        marginTop:7
    },
    logoUsuario:{
        height: '85%',
        width: '34%',
        marginTop:5
    },

    formulario:{
        width: '70%',
        flexDirection:'row',
        marginTop:15
    },

    logoFormularioView:{
        marginTop:20,
        height: 15,
        width: 15,
        marginLeft: 15

    },
    textoFormularioView:{
        height: '15%',
        width: 6,
        borderBottomWidth : 0
    },
    
    textoContraseña:{
        marginTop:10,
        marginLeft:10
    },
    touchable:{
        marginTop:25,
        height:'20%',
        width:'60%',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor: 'rgba(0,0,0,0.2)',
        fontFamily:''     
    }
}); 

export default ModalInitLead
/**
 * npm install intl -f
 */
import 'intl';
import 'intl/locale-data/jsonp/en';

exports.nombrebd = '/Gestor.db';
exports.urlSystem = 'https://checgiipandroid.chec.com.co/RPC2PRD.ashx';
//exports.urlSystem = "http://webservices.enerca.com.co:8069/xmlrpc/2/object";
//exports.urlSystem = "http://190.249.138.220/website/RPC2PRD.ashx";

//exports.urlSystem = "http://190.66.8.82:8182/WsSismovilAndroid.asmx/";
//exports.urlSystem = "https://checgiipandroiddes.chec.com.co/rpc2.ashx";

//exports.versionGesper = "Redes";
exports.versionGesper = 'Comercial';

exports.bdxmlrpc='';

exports.version = 10.6;
exports.idUsuario = 'idUsuario';
exports.login= 'login';
exports.password='password';

exports.reintentos = 0; //variable para manejar los reintentos de las ordenes
exports.tamanoenvio = 10; //variable para manejar el tamaño del envio
exports.parametrizacionReintentos = 5; //variable para manejar los reintentos de las ordenes
exports.impresora; //variable almacenar la impresora
exports.guardar = false; //variable para preguntar si desea salir sin guardar

exports.altoTablaExpandida = 100;
exports.altoTablaEncogida = 35;
//exports.urlSystem = "www.reactiva.com.co:8088/WsSismovilAndroid.asmx/";

exports.parametrizacionBufferBajada = 'BufferBajada';
exports.parametrizacionBufferSubida = 'BufferSubida';
exports.parametrizacionBufferParametros = 'BufferParametros';

exports.valorBufferBajada;
exports.valorBufferSubida;
exports.valorBufferParametros;

exports.acumuladoVariables = null; //variable para acumular el valor de los detalles
//exports.siGuardo; // variable para saber si va a perder datos
//exports.menuItemIndex; // variable para saber cual fue el menu que se selecciono

exports.objetofiltro;

exports.activo = 'activo';
exports.pendiente = 'pendiente';
exports.confirmado = 'confirmado';
exports.confirmadoMovil = 'confirmadoMovil';
exports.confirmadoServidor = 'confirmadoServidor';
exports.terminado = 'terminado';
exports.ejecucion = 'ejecucion';
exports.servidor = 'servidor';
exports.traslado = 'trasladado';
exports.inactivo = 'iinactivo';
exports.parametrizacionIMPRESORA = 'IMPRESORA';
exports.enterImpresion = 'ENTERIMPRESION';
exports.documentoMateriales = 'MATERIALES DEFINITIVOS';
exports.ordenamiento = 'asc';

var vistaActiva; //vista en la que esta ubicado actualmente

exports.manejadorButton = function (unButton, unEstado, unaActivityIndicator) {
  Alloy.Globals.ultimaFechaEvento = new Date();
  if (unEstado) {
    //Titanium.API.debug("lo escondo y prendo el boton");
    unaActivityIndicator.hide();
    unaActivityIndicator.width = Ti.UI.SIZE;
    unaActivityIndicator.height = Ti.UI.SIZE;
    unButton.enabled = true;
    unButton.opacity = 1.0;
  } else {
    //Titanium.API.debug("lo muestro y lo apago");
    unaActivityIndicator.show();
    unaActivityIndicator.width = '100%';
    unaActivityIndicator.height = '100%';
    unButton.enabled = false;
    unButton.opacity = 0.5;
  }
};
//var gestor = require('validacionGestor');

exports.actualizacionTrabajoOrden = function (estado) {
  if (estado == null) {
    estado = gestor.trabajoOrdenEstadoEjecutado;
  }
  if (
    gestor.ordenTrabajo != undefined &&
    gestor.ordenTrabajo.get('idOrden') != undefined
  ) {
    if (estado == exports.documentoMateriales) {
      var documento = Alloy.createCollection('documento');
      documento &&
        documento.fetch({
          query:
            ' SELECT distinct documento.id as identificador, documento.descripcion FROM documento, trabajoImpresion ' +
            ' where trabajoImpresion.id_documento=documento.id ' +
            "and documento.descripcion= '" +
            exports.documentoMateriales +
            "'" +
            "and trabajoImpresion.id_trabajo= '" +
            gestor.trabajoOrden.get('idTrabajo') +
            "'",
        });
      //console.log("documento.length  ",documento.length , "  ",estado);
      if (documento.length == 0) {
        estado = gestor.trabajoOrdenEstadoEjecutado;
      }
    }
    var trabajoOrden = Alloy.createCollection('trabajoOrden');
    trabajoOrden &&
      trabajoOrden.fetch({
        query:
          'select trabajoOrden.* from trabajoOrden where trabajoOrden.idOrden=' +
          gestor.ordenTrabajo.get('idOrden') +
          '',
      });

    if (trabajoOrden.length > 0) {
      //console.log("  ",trabajoOrden.models[0].get("estadoTrabajoOrden"), "     ",exports.documentoMateriales);
      if (
        trabajoOrden.models[0].get('estadoTrabajoOrden') !=
        exports.documentoMateriales
      ) {
        trabajoOrden.models[0]
          .set({
            estado: estado,
            estadoTrabajoOrden: estado,
          })
          .save();
      }
    }
  }
};

//la notificacion para sacar el mensaje
exports.notificacion = function (mensaje) {
  var toast = Ti.UI.createNotification({
    message: mensaje,
    duration: 0.5, //Ti.UI.NOTIFICATION_DURATION_SHORT,
  });
  toast.show();
  toast = null;
};

//variable para darle manejo a el filtro
exports.objetofiltro;

//funcion para esconder el teclado
exports.esconderTeclado = function () {
  if (Ti.Platform.osname === 'android') {
    Ti.UI.Android.hideSoftKeyboard();
  } else {
    for (var i = 0; i < textFields.length; i++) {
      textFields[i].blur();
    }
  }
};

//funcion para abrir el teclado
exports.abrirTeclado = function () {
  if (Ti.Platform.osname === 'android') {
    Ti.UI.Android.showSoftKeyboard();
  } else {
    for (var i = 0; i < textFields.length; i++) {
      textFields[i].blur();
    }
  }
};

//se abre una ventana
exports.functionAbrirVentana = function (nombreVentana, args) {
  /*var win2 = Alloy.createController(nombreVentana,args).getView();
	 win2.open();*/
  Alloy.Globals.nombreWindow = Alloy.createController(
    nombreVentana,
    args,
  ).getView();
  Alloy.Globals.nombreWindowIncluir = Alloy.Globals.nombreWindow;
  Alloy.Globals.nombreWindow.open();
};
/**exports.incluirVentana = function(src, param, args){
	param.removeAllChildren();
	Alloy.Globals.nombreWindowIncluir = Alloy.createController(src, args).getView();
	Alloy.Globals.nombreWindow = Alloy.createController(src, args).getView();
	////console.log("Incluir INDEX",Alloy.Globals.nombreWindow); 
	param.add(Alloy.Globals.nombreWindow);
};**/

exports.incluirVentana = function (src, param, args) {
  param.removeAllChildren();
  Alloy.Globals.nombreWindow = Alloy.createController(src, args).getView();
  Alloy.Globals.nombreWindowIncluir = Alloy.Globals.nombreWindow;

  ////console.log("Incluir INDEX",Alloy.Globals.nombreWindow);
  param.add(Alloy.Globals.nombreWindow);
};

exports.formatoFecha = function (una_fecha) {
  var fecha = una_fecha == null ? new Date() : una_fecha;

  var dia =
    fecha.getDate().toString().length == 1
      ? '0' + fecha.getDate()
      : fecha.getDate();
  var mes =
    (fecha.getMonth() + 1).toString().length == 1
      ? '0' + (fecha.getMonth() + 1)
      : fecha.getMonth() + 1;
  var horas =
    fecha.getHours().toString().length == 1
      ? '0' + fecha.getHours()
      : fecha.getHours();
  var minutos =
    fecha.getMinutes().toString().length == 1
      ? '0' + fecha.getMinutes()
      : fecha.getMinutes();
  var segundos =
    fecha.getSeconds().toString().length == 1
      ? '0' + fecha.getSeconds()
      : fecha.getSeconds();
  var miFecha =
    '' +
    fecha.getFullYear() +
    '-' +
    mes +
    '-' +
    dia +
    ' ' +
    horas +
    ':' +
    minutos +
    ':' +
    segundos +
    '';

  return miFecha;

  // var fecha = String.formatDate(mi_fecha, 'medium');
  // var hora = String.formatTime(mi_fecha, 'medium');
  // var fechaCompleta = (fecha) + ' ' + (hora);
  //
  //	return fechaCompleta
};
exports.formatoFechaCorta = function (una_fecha) {
  var fecha = una_fecha == null ? new Date() : una_fecha;
  var dia =
    fecha.getDate().toString().length == 1
      ? '0' + fecha.getDate()
      : fecha.getDate();
  var mes =
    (fecha.getMonth() + 1).toString().length == 1
      ? '0' + (fecha.getMonth() + 1)
      : fecha.getMonth() + 1;
  var miFecha = fecha.getFullYear() + '-' + mes + '-' + dia;
  return miFecha;
  // var fecha = String.formatDate(mi_fecha, 'medium');
  // var hora = String.formatTime(mi_fecha, 'medium');
  // var fechaCompleta = (fecha) + ' ' + (hora);
  //
  //	return fechaCompleta
};

exports.formatoFecha12 = function (una_fecha) {
  var fecha = una_fecha == null ? new Date() : una_fecha;

  var dia =
    fecha.getDate().toString().length == 1
      ? '0' + fecha.getDate()
      : fecha.getDate();
  var mes =
    (fecha.getMonth() + 1).toString().length == 1
      ? '0' + (fecha.getMonth() + 1)
      : fecha.getMonth() + 1;
  var hora12 =
    parseInt(fecha.getHours()) > 12
      ? parseInt(fecha.getHours()) - 12
      : fecha.getHours();
  var minutos =
    fecha.getMinutes().toString().length == 1
      ? '0' + fecha.getMinutes()
      : fecha.getMinutes();
  hora12 = hora12.toString().length == 1 ? '0' + hora12 : hora12;
  var horario = parseInt(fecha.getHours()) >= 12 ? 'PM' : 'AM';
  var miHora12 = hora12 + ':' + minutos + ' ' + horario;
  var miFecha = fecha.getFullYear() + '-' + mes + '-' + dia + ' ' + miHora12;

  return miFecha;

  // var fecha = String.formatDate(mi_fecha, 'medium');
  // var hora = String.formatTime(mi_fecha, 'medium');
  // var fechaCompleta = (fecha) + ' ' + (hora);
  //
  //	return fechaCompleta
};

exports.formatoHora12 = function (una_fecha) {
  var fecha = una_fecha == null ? new Date() : una_fecha;
  var hora12 =
    parseInt(fecha.getHours()) > 12
      ? parseInt(fecha.getHours()) - 12
      : fecha.getHours();
  var minutos =
    fecha.getMinutes().toString().length == 1
      ? '0' + fecha.getMinutes()
      : fecha.getMinutes();
  hora12 = hora12.toString().length == 1 ? '0' + hora12 : hora12;
  var horario = parseInt(fecha.getHours()) >= 12 ? 'PM' : 'AM';
  var miHora12 = hora12 + ':' + minutos + ' ' + horario;

  return miHora12;

  // var fecha = String.formatDate(mi_fecha, 'medium');
  // var hora = String.formatTime(mi_fecha, 'medium');
  // var fechaCompleta = (fecha) + ' ' + (hora);
  //
  //	return fechaCompleta
};

/* params unNumero es un numero a formatear
 * Funcion para formatear el numero a un formato de moneda
 */
exports.formatMoneda = function (e) {
  console.log(typeof(e), e);
  return '$ ' + new Intl.NumberFormat('en-US').format(parseInt(e?e:0));
};

/* params unNumero es un numero a formatear
 * Funcion para formatear el numero a un formato de moneda
 */
exports.formatMonedaSinSigno = function (e) {
  return new Intl.NumberFormat('en-US').format(parseInt(e?e:0));
};

exports.pasarListaString = function (coleccion, atributo) {
  var listaString = '';
  for (var i = 0; i < coleccion.length; i++) {
    listaString += coleccion.models[i].get(atributo) + ' , ';
  }

  if (coleccion.length > 0) {
    listaString = listaString.substring(0, listaString.length - 2);
  }
  return listaString;
};

// /* params evento object de donde se ejecuto
// * modal = indica si es una modal o no
// * function que sale de la ventana o pasa a la siguiente ventana dependiendo si es modal o no
// */
exports.SalirVentana = function (
  unEvento,
  unaWindow,
  unaActivityIndicator,
  itemIndex,
) {
  var posicionMenu = this.menuPrincipal.itemActual;
  //console.log("itemIndex  ",itemIndex);
  if (itemIndex != undefined && itemIndex != null) {
    posicionMenu = itemIndex;
  }
  exports.manejadorButton(unEvento, false, unaActivityIndicator);
  //unaWindow.close();
  var row = this.menuPrincipal.sections[0].getItemAt(posicionMenu);
  row.seleccionado.color = '#00C600';
  row.seleccionado.text = '\uf4c9';
  this.menuPrincipal.sections[0].updateItemAt(posicionMenu, row);
  //var gestor = require('validacionGestor');
  var estadoMenuOrdenTrabajo = Alloy.createCollection('estadoMenuOrdenTrabajo');
  estadoMenuOrdenTrabajo &&
    estadoMenuOrdenTrabajo.fetch({
      query:
        "select * from estadoMenuOrdenTrabajo where idOrden = '" +
        gestor.ordenTrabajo.get('idOrden') +
        "' and idTrabajo = '" +
        gestor.trabajoOrden.get('idTrabajo') +
        "' and menu = '" +
        row.properties.itemId +
        "'",
    });
  if (estadoMenuOrdenTrabajo.models.length > 0) {
    estadoMenuOrdenTrabajo.models[0]
      .set({
        estado: 'completo',
      })
      .save();
  }
  exports.guardar = false; //como se guardo entonces se apaga la variable de control de guardado
  this.menuPrincipal.fireEvent('moverMenu');
  exports.manejadorButton(unEvento, true, unaActivityIndicator);
};
//
// /* params evento object de donde se ejecuto
// * modal = indica si es una modal o no
// * function que sale de la ventana o pasa a la siguiente ventana dependiendo si es modal o no
// */
exports.SalirVentanaModal = function (
  unEvento,
  unaWindow,
  unaActivityIndicator,
) {
  exports.manejadorButton(unEvento, false, unaActivityIndicator);
  unaWindow.close();
  exports.guardar = true; //se guarda en la modal y se pone en true para verificar
  exports.manejadorButton(unEvento, true, unaActivityIndicator);
};
//
//
// /* params unNumero es un numero a formatear
// * Funcion para formatear el numero a dos decimales
// */
// exports.formatDecimal  = function (e) {
// return parseFloat(Math.round((e)*100)/100);
// };
//
//
exports.seleccionarPicker = function (miPicker, textBusqueda) {
  for (var i = 0; i < miPicker.columns[0].rows.length; i++) {
    if (miPicker.columns[0].rows[i].title == textBusqueda) {
      miPicker.setSelectedRow(0, i, false);
      i = miPicker.columns[0].rows.length;
    }
  }
};
//
//
exports.addColumn = function (tblName, newFieldName, colSpec) {
  var nombreBaseDatos = exports.nombrebd.substring(
    1,
    exports.nombrebd.length - 3,
  );
  var db = Ti.Database.open(nombreBaseDatos);
  var fieldExists = false;
  resultSet = db.execute('PRAGMA TABLE_INFO(' + tblName + ')');
  if (resultSet != null) {
    while (resultSet.isValidRow()) {
      if (resultSet.field(1) == newFieldName) {
        fieldExists = true;
      }
      resultSet.next();
    } // end while
  }
  if (!fieldExists) {
    // field does not exist, so add it
    db.execute(
      'ALTER TABLE ' + tblName + ' ADD COLUMN ' + newFieldName + ' ' + colSpec,
    );
  }
  db.close();
};

exports.crearTabla = function (nombreTabla, campos) {
  var nombreBaseDatos = exports.nombrebd.substring(
    1,
    exports.nombrebd.length - 3,
  );
  var db = Ti.Database.open(nombreBaseDatos);
  db.execute(
    'CREATE TABLE IF NOT EXISTS ' + nombreTabla + ' (' + campos + ');',
  );
  db.close();
};

import React, { useState } from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import Modal from "react-native-modal";
import { Options } from '../picker/components/options';
export const SearchPicker = (props) => {
    const { items = timeOptions, value = items[0] ?? '', onChange = () => { }, style = { button: {} } } = props
    const [showOptions, setShowOptions] = useState(false);
    const selectItem = (item) => {
        onChange(item);
        setShowOptions(false);
    }
    return (
        <>
            <TouchableOpacity onPress={() => { setShowOptions(true) }} style={[styles.button, style.button]}>
                <Text style={{ width:'80%', alignSelf: 'center', marginLeft: 16, fontSize: 18 }}>{value.label}</Text>
                <Icon name="search"  size={16} color='#fff' containerStyle={{ width: '20%', paddingRight: 16, marginTop: 0, justifyContent: 'center' }}></Icon>
            </TouchableOpacity>
            <Modal
                style={{ flex: 1 }}
                isVisible={showOptions}
                onRequestClose={() => {
                    setShowOptions(false);
                }}
                onBackButtonPress={() => {
                    setShowOptions(false);
                }}
                onBackdropPress={() => {
                    setShowOptions(false);
                }}
            >
                <Options data={items} onPress={selectItem}></Options>
            </Modal>
        </>
    )
}
export const SearchPickerEconomi = (props) => {
    const { items = timeOptions, value = items[0] ?? '', onChange = () => { }, style = { button: {} } } = props
    const [showOptions, setShowOptions] = useState(false);
    const selectItem = (item) => {
        onChange(item);
        setShowOptions(false);
    }
    return (
        <>
            <TouchableOpacity onPress={() => { setShowOptions(true) }} style={[styles.button2, style.button2]}>
                <Text style={{ width:'80%', alignSelf: 'center', marginLeft: 16, fontSize: 18 }}>{value.label}</Text>
                <Icon name="sort-down" type="font-awesome-5" size={16} color='#606060' containerStyle={{ width: '20%', paddingRight: 16, marginTop: 0, justifyContent: 'center' }}></Icon>
            </TouchableOpacity>
            <Modal
                style={{ flex: 1 }}
                isVisible={showOptions}
                onRequestClose={() => {
                    setShowOptions(false);
                }}
                onBackButtonPress={() => {
                    setShowOptions(false);
                }}
                onBackdropPress={() => {
                    setShowOptions(false);
                }}
            >
                <Options data={items} onPress={selectItem}></Options>
            </Modal>
        </>
    )
}
const styles = StyleSheet.create({
    button: { borderWidth: 1, marginHorizontal: 5, borderRadius: 10, justifyContent: 'space-between', height: 50, flexDirection: 'row' },
    button2: { borderWidth: 1, marginHorizontal: 5, borderRadius: 10, justifyContent: 'space-between', height: 100, flexDirection: 'row' }

})
export const NumberWithCommas = (value) => {
	if (value) {
		return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	return "0"
}

export const FormatMoney = (value) => {
	return '$ ' + NumberWithCommas(value);
};

export const FormatUsa = (value) => {
	return 'USD ' + NumberWithCommas(value);
};



export const FormatShortDateUTC = (date, withName) => {
	var _date = date == null ? new Date() : date;
	var _withName = withName == null ? false : withName;
	if (_withName) {
		return ((((_date.getUTCDate()).toString().length == 1) ? "0" + (_date.getUTCDate()) : (_date.getUTCDate())) + " de " +
			monthsSpanish(_date.getUTCMonth() + 1) + " de " +
			_date.getUTCFullYear());
	} else {
		return (_date.getUTCFullYear()) + "-" +
			(((_date.getUTCMonth() + 1).toString().length == 1) ? "0" + (_date.getUTCMonth() + 1) : (_date.getUTCMonth() + 1)) + "-" +
			(((_date.getUTCDate()).toString().length == 1) ? "0" + (_date.getUTCDate()) : (_date.getUTCDate()));
	}


};

const monthsSpanish = (month) => {
	months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
	if (month > 0 && month < 13) {
		return months[month];
	} else {
		return month
	}
}

export const FormatDateComplete = function (una_fecha) {
	const fecha = una_fecha ? new Date(una_fecha) : new Date(Date.now());
	const miFecha = fecha.getFullYear() + "-" + (((fecha.getMonth() + 1).toString().length == 1) ? "0" + (fecha.getMonth() + 1) : (fecha.getMonth() + 1)) + "-" + (((fecha.getDate()).toString().length == 1) ? "0" + (fecha.getDate()) : (fecha.getDate()));
	return miFecha;
};

export const FormatParsedDate = (strDate) 	=>	{ 
	var strSplitDate = String(strDate).split(' ');
    var date = new Date(strSplitDate[0]);
    var dd = date.getDate();
    var mm = date.getMonth() + 1; 
    var yyyy = date.getFullYear();
    	if (dd < 10) {
        	dd = '0' + dd;
    	}
    	if (mm < 10) {
        	mm = '0' + mm;
    	}
    	date =  dd + "-" + mm + "-" + yyyy;
    return date.toString();
}
export const OdooDateToReact = (dateOdoo) => {
    const dateSplit = dateOdoo.split(" ")[0].split("-");
    const hourSplit=dateOdoo.split(" ")[1].split(":");
    return new Date(dateSplit[0],dateSplit[1],dateSplit[2],hourSplit[0],hourSplit[1],hourSplit[2]);
}
export const OdooErrorToReact = (error) => {
	const errorSplit = error.split("'")[1];
	return errorSplit;
}
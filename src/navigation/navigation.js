import React from 'react';
import {createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from '../screens/LoginScreen';
import MenuScreen from '../screens/MenuScreen';
import CreateClient from '../components/CreatePerson/CreateClient';
import CreateUser from '../components/CreatePerson/CreateUser';
import CreateProvider from '../components/CreatePerson/CreateProvider';
import BoxDetails from '../components/Box/BoxDetails';
import BoxAddDetails from '../components/Box/BoxAddDetails';
import BoxGeneral from '../components/Box/BoxGeneral';
import Pay from '../components/TypePay/Pay';
import TypeSpent from '../components/TypeProduct/TypeSpent';
import Invoices from '../components/invoices/invoice';
import CreateInvoice from '../components/invoices/components/createInvoiceSupplier';
import CreatePay from '../components/TypePay/components/createpay'
import QuotationList from '../components/TypePay/quotations/components/quotationsList';

const Stack = createNativeStackNavigator();

export default function navigation() {
  return (
    <Stack.Navigator
    initialRouteName='Login'>
        <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          headerShown:false,
          headerTintColor:'#fff'}}/>
        <Stack.Screen
         name='Usuario'
         component={CreateUser}
         options={{
          headerTintColor:'#fff',
          headerShown:false,}}/>
         <Stack.Screen
        name="Menu"
        component={MenuScreen}
        options={{
          headerShown:false,
          headerTintColor:'#fff'}}/>
         <Stack.Screen
        name="Cliente"
        component={CreateClient}
        options={{
          headerShown:false,
          headerTintColor:'#fff'}}/>
        <Stack.Screen
        name="Proveedor"
        component={CreateProvider}
        options={{
          headerShown:false,
          headerTintColor:'#fff'}}/>
         <Stack.Screen
         name='Cajas'
         component={BoxGeneral}
         options={{
          headerTintColor:'#fff',
          headerStyle: {backgroundColor: '#3A86FF'}}}/>
         <Stack.Screen
         name='DetalleCaja'
         component={BoxDetails}
         options={{
          headerTintColor:'#fff',
          headerStyle: {backgroundColor: '#3A86FF'}}}/>
          <Stack.Screen
          name='tipoProducto'
          component={BoxAddDetails}
          options={{
            headerShown:false,
            headerTintColor:'#fff'}}/>
          <Stack.Screen
          name='Gasto'
          component={TypeSpent}
          options={{
            headerTintColor:'#fff',
            title: 'Agregar Gasto',
            headerStyle: {backgroundColor: '#3A86FF',
            headerTintColor:'#fff',
            }}}/>
          <Stack.Screen
          name='Pago'
          component={Pay}
          options={{
            headerTintColor:'#fff',
            title: 'Pagos',
            headerStyle: {backgroundColor: '#3A86FF',
            headerTintColor:'#fff',
            }}}/>
          <Stack.Screen
          name='Facturas'
          component={Invoices}
          options={{
            headerShown:false,
            headerTintColor:'#fff'}}/>
          <Stack.Screen
          name='crearFactura'
          component={CreateInvoice}
          options={{
            headerShown:false,
            headerTintColor:'#fff'}}/>
          <Stack.Screen
          name='crearPago'
          component={CreatePay}
          options={{
            headerTintColor:'#fff',
            title: 'Agregar Pago',
            headerStyle: {backgroundColor: '#3A86FF',
            headerTintColor:'#fff',
            }}}/>
          <Stack.Screen
          name='Quotations'
          component={QuotationList}
          options={{
            headerTintColor:'#fff',
            title: 'Agregar Pago',
            headerStyle: {backgroundColor: '#3A86FF',
            headerTintColor:'#fff',
            }}}/>
    </Stack.Navigator>
  )
}
import Odoo from "react-native-odoo";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {OdooConfig} from '../utils/odoo-config';


export const CreateBox = async (data,callback) => {
/*   let data ={
    diario_id:7
  } */
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password, 
  });
  const params = {
    model: 'epii.caja.webservice',
    method: 'crearCaja',
    args: args,
    kwargs: {},
    };
    client.connect((err, response) => {
      console.log('realize la conexion')
      if (err) {
        callback(err, false);
        return;
      }
      client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
        console.log('realice createBox',err,response)
        console.log('datos',typeof err)
        if (err && err != null) {
          console.log('Error', err);
          callback(false, err);
          return false;
        }else{
           callback(true, response);
        console.log('cree la caja')
        }
      });
    });
  };

  export const OpenBox = async (data,callback) => {
   /*  let data ={
      id:1
  } */
    const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
    const args = [0, data];
    const config = await OdooConfig();
    const client = new Odoo({
      host: config.host,
      port: config.port,
      database: config.database,
      username: credentials.username,
      password: credentials.password,
    });
    const params = {
      model: 'epii.caja.webservice',
      method: 'abrirCaja',
      args: args,
      kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          console.log('response openBox',response)
          callback(true, response);
        });
      });
    };

export const CloseBox = async (data,callback) => {
 /*  let data ={
    id:1
} */
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
  const params = {
    model: 'epii.caja.webservice',
    method: 'cerrarCaja',
    args: args,
    kwargs: {},
    };
    client.connect((err, response) => {
      if (err) {
        callback(err, false);
        return;
      }
      client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
        if (err) {
          console.log('Error', err);
          callback(false, err);
          return false;
        }
        callback(true, response);
      });
    });
  };

export const ListBox = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
  const params = {
    model: 'epii.caja.webservice',
    method: 'listarCajas',
    args: args,
    kwargs: {},
    };
    client.connect((err, response) => {
      console.log('genero conexion')
      if (err) {
        console.log('genero error')
        callback(err, false);
        return;
      }
      client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
       /*  console.log('listar caja', err,response) */
        if (err) {
          console.log('error listBox')
          callback(false, err);
          return false;
        }
        console.log('response listBox')
        callback(true, response);
      });
    });
  };

export const GetDetailBox = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
  const params = {
    model: 'epii.caja.webservice',
    method: 'obtenerDetalleCaja',
    args: args,
    kwargs: {},
    };
    client.connect((err, response) => {
      if (err) {
        callback(err, false);
        return;
      }
      client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
        if (err) {
          console.log('Error', err);
          callback(false, err);
          return false;
        }
        callback(true, response);
      });
    });
  };

export const CreateInvoiceSupplier = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
  const params = {
    model: 'epii.caja.webservice',
    method: 'crearFacturaProveedor',
    args: args,
    kwargs: {},
    };
    client.connect((err, response) => {
      if (err) {
        callback(err, false);
        return;
      }
      client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
        if (err) {
          console.log('Error', err);
          callback(false, err);
          return false;
        }
        callback(true, response);
      });
    });
  };


export const UploadTransaction = async (data,callback) => {
  console.log("entre UploadTransaction",data)
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
  const params = {
    model: 'epii.caja.webservice',
    method: 'subirTransaccion',
    args: args,
    kwargs: {},
    };
    client.connect((err, response) => {
      console.log('conexion en UploadTransaction',response)
      if (err) {
        callback(err, false);
        return;
      }
      client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
        console.log('rpc_call en UploadTransaction')
        if (err) {
          console.log('Error', err);
          callback(false, err);
          return false;
        }
        console.log('callback en UploadTransaction')
        callback(true, response);
      });
    });
  };
  
   
     export const CreateProducts = async (data,callback) => {
      /*  let data ={
         name:"producto 1"
         tipo: "gasto"
     } */
       const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
       const args = [0, data];
       const config = await OdooConfig();
       const client = new Odoo({
         host: config.host,
         port: config.port,
         database: config.database,
         username: credentials.username,
         password: credentials.password, 
       });
       const params = {
         model: 'crmbackend.webservice',
         method: 'crearProductos', 
         args: args,
         kwargs: {},
         };       
         client.connect((err, response) => {
           if (err) {
             callback(err, false);
             return;
           }
           client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
            console.log('entro a error')
             if (err) {
               callback(false, err);
               return false;
             }
             console.log('respuesta',response)
             callback(true, response);
           });
         });
       };
   
import { ToastAndroid } from "react-native";
import Odoo from "react-native-odoo";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {OdooConfig} from "../utils/odoo-config";
import { CreateBox,OpenBox } from "./box.services";


export const AuthService = async (data,callback) => {
  const args = [0];
  await AsyncStorage.setItem('credentials', JSON.stringify(data))
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: data.username,
    password: data.password, 
  });
  const params = {
    model: 'crmbackend.webservice',
    method: 'retornardatos',
    args: args,
    kwargs: {},
  };
  client.connect(async (err, response) => {
    console.log("se esta conectando")
    if (err) {
      if (err.data && err.message) {
        ToastAndroid.show("Usuario y/o contraseña incorrecta", ToastAndroid.SHORT)
        callback(response, false)
      } else {
        ToastAndroid.show("Error en el servidor", ToastAndroid.SHORT)
        callback(response, false)
      }   
    }else{
      await AsyncStorage.setItem('Session', JSON.stringify(client));
      await AsyncStorage.setItem('uid', JSON.stringify(response.uid));
      client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
        if (err) {
          console.log('Error', err);
          callback(false, err);
          return false;
        }
       /*  console.log("retorno datos",response) */
        callback(response, true);
      });

      
    }
      
  });
};

export const ListLeads = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
      });
      const params = {
        model: 'crmbackend.webservice',
        method: 'listarleads',
        args: args,
        kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          callback(true, response);
        });
      });
};

export const ListQuotations = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
      });
      const params = {
        model: 'crmbackend.webservice',
        method: 'listarCotizaciones',
        args: args,
        kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          callback(true, response);
        });
      });
};

export const ListInvoice = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
      });
      const params = {
        model: 'epii.caja.webservice',
        method: 'listarFacturas',
        args: args,
        kwargs: {},
      };
      client.connect((err, connect) => {
        console.log("ListInvoice connect",connect)
        if (err) {
          console.log("ListInvoice err")
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          console.log("ListInvoice rpc_call",response)
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          console.log("ListInvoice callback",callback(true, response))
          callback(true, response);
        });
      });
};

export const CreateInvoice = async(data,callback) =>{
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
      });
      const params = {
        model: 'epii.caja.webservice',
        method: 'crearFactura',
        args: args,
        kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          callback(true, response);
        });
      });
};

export const CreateInvoiceDeposit = async(data,callback) =>{
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
      });
      const params = {
        model: 'epii.caja.webservice',
        method: 'crearFacturaAbono',
        args: args,
        kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          callback(true, response);
        });
      });
};

export const CreateInvoiceProvider = async(data,callback) =>{
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
      });
      const params = {
        model: 'epii.caja.webservice',
        method: 'crearFacturaProveedor',
        args: args,
        kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          callback(true, response);
        });
      });
};

export const RecordTime = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
      });
      const params = {
        model: 'crmbackend.webservice',
        method: 'registrartiempo',
        args: args,
        kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          callback(true, response);
        });
      });
};

export const CreateQuotations = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password
      });
      const params = {
        model: 'crmbackend.webservice',
        method: 'crearCotizacion',
        args: args,
        kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          console.log("Response=>",response)
          callback(true, response);
        });
      });
};

export const ReturnData = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password
      });
      const params = {
        model: 'crmbackend.webservice',
        method: 'retornardatos',
        args: args,
        kwargs: {},
      };
      client.connect((err, response) => {
        if (err) {
          callback(err, false);
          return;
        }
        client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
          if (err) {
            console.log('Error', err);
            callback(false, err);
            return false;
          }
          callback(true, response);
        });
      });
};
   
export const createResPartner = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
  const params = {
    model: 'crmbackend.webservice',
    method: 'crearrespartner',
    args: args,
    kwargs: {},
  };
  client.connect((err, response) => {
    console.log('realiza la conexion')
    if (err) {
      console.log("error de conexion",err)
      callback(err, false);
      return;
    }
    client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
      console.log('entra en rpc_call')
      if (err) {
        console.log('genera error')
        console.log('Error', err);
        callback(false, err);
        return false;
      }
      console.log("Response=>",response)
      callback(true, response);
    });
  });
};

export const createLead = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
  const params = {
    model: 'crmbackend.webservice',
    method: 'crearlead',
    args: args,
    kwargs: {},
  };
  client.connect((err, response) => {
    if (err) {
      console.log("error de conexion",err)
      callback(err, false);
      return;
    }
    client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
      if (err) {
        console.log('Error', err);
        callback(false, err);
        return false;
      }
      console.log("Response=>",response)
      callback(true, response);
    });
  });
};

export const editLead = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0,data];
  const config = await OdooConfig();          
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
         
  const params = {
    model: 'crmbackend.webservice',
    method: "modificarlead",
    args: args,
    kwargs: {},
  };
  client.connect((err, response) => {
    if (err) {
      callback(err, false);
      return;
    }
    client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
      if (err) {
        console.log('Error', err);
        callback(false, err);
        return false;
      }
      console.log("Response=>",response)
      callback(true, response);
    });
  });
};

export const CreateCreditNote = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0,data];
  const config = await OdooConfig();          
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
         
  const params = {
    model: 'crmbackend.webservice',
    method: "crearFacturaAbono",
    args: args,
    kwargs: {},
  };
  client.connect((err, response) => {
    if (err) {
      callback(err, false);
      return;
    }
    client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
      if (err) {
        console.log('Error', err);
        callback(false, err);
        return false;
      }
      console.log("Response=>",response)
      callback(true, response);
    });
  });
};

export const confirmQuotation = async (data,callback) => {
  const credentials = JSON.parse(await AsyncStorage.getItem('credentials'))
  const args = [0, data];
  const config = await OdooConfig();
  const client = new Odoo({
    host: config.host,
    port: config.port,
    database: config.database,
    username: credentials.username,
    password: credentials.password,
  });
  const params = {
    model: 'crmbackend.webservice',
    method: 'confirmarCotizacion',
    args: args,
    kwargs: {},
  };
  client.connect((err, response) => {
    if (err) {
      console.log("error de conexion",err)
      callback(err, false);
      return;
    }
    client.rpc_call('/web/dataset/call_kw', params, (err, response) => {
      if (err) {
        console.log('Error', err);
        callback(false, err);
        return false;
      }
      console.log("Response=>",response)
      callback(true, response);
    });
  });
};


      
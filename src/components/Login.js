import React, { useState } from 'react';
import {  Button,
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import { AuthService} from '../services/services'
import {useNavigation} from '@react-navigation/native'


export default function LoginForm() {
    const [error,setError] = useState()
    const navigation = useNavigation();

  const goToMenu = () =>{
    navigation.navigate('Menu',{})
  }

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    validateOnChange: false,
    onSubmit: formValue => {
      setError('');
      const {username, password} = formValue;
      getUserDataDB(username, password);
    },
  });

  async function getUserDataDB(username, password){
/*     ToastAndroid.show("Iniciando sesion", ToastAndroid.LONG); */
    await AuthService({username, password},async (flag,res)=>{
    if(flag){
      goToMenu()
    }})
    
  }

  const goToUser =(Item)=>{
    console.log("entramos a creacion de usuario",Item)
    navigation.navigate('Usuario',{register:Item})
  }

   return ( 
      <View style={styles.container}>
         <Text style={{alignSelf:'center',fontSize:30,color:'#1982C4',fontWeight:'bold'}}>Iniciar Sesion</Text>
       <View style={styles.body}>
          <View style={styles.sectionForm}>
            <View style={styles.sectionInput}>
              <TextInput
                placeholder="Nombre de usuario"
                autoCapitalize="none"
                value={formik.values.username}
                onChangeText={text => formik.setFieldValue('username', text)}
              />
            </View>
            <View style={styles.sectionInput}>
              <TextInput
                placeholder="Contraseña"
                autoCapitalize="none"
                secureTextEntry={true}
                value={formik.values.password}
                onChangeText={text => formik.setFieldValue('password', text)}
              />
            </View>
            <View style={{width:'50%',alignSelf:'center'}}>
              <Button
                title="INGRESAR"
                onPress={formik.handleSubmit}

              />
            <TouchableOpacity style={{alignSelf:'center',marginTop:'10%'}} onPress={()=>goToUser('usuario')}>
              <Text style={{color:'#1982C4',borderBottomWidth:1,borderBottomColor:'#1982C4'}}>Crear Usuario</Text>
            </TouchableOpacity>
              <Text style={styles.error}>{formik.errors.username}</Text>
              <Text style={styles.error}>{formik.errors.password}</Text>
              <Text style={styles.error}>{error}</Text>
            </View>
          </View>
        </View>         
      </View>
      
  );
}
function initialValues() {
  return {
    username: 'admin',
    password: 'admin',
  };
}

function validationSchema() {
  return {
    username: Yup.string().required('El usuario es obligatorio'),
    password: Yup.string().required('La contraseña es obligatoria'),
  };
}


const styles = StyleSheet.create({
  container: {
    justifyContent:'center',
    height:'100%',
    backgroundColor: '#FFFFFF',
  },
  sectionForm:{
    alignSelf: 'center',
    width:'80%'
  },
  sectionInput:{
    borderBottomWidth:1,
    borderBottomColor:'black',
    marginBottom:15}
})
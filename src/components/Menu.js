import { View,
    Text,
    TouchableOpacity,
   } from 'react-native'
  import React,{useState}from 'react'
  import {Button} from 'react-native-elements'
  import {useNavigation} from '@react-navigation/native'; 
  import Icon from 'react-native-vector-icons/FontAwesome';
  
  export default function Menu(props) {
    const navigation = useNavigation();
    const [provider, setProvider] = useState("in_invoice")
    const [client, setClient] = useState("out_invoice")

    const goToClient = (Item) => {
      console.log('entre a creacion cliente',Item)
      navigation.navigate('Cliente',{register:Item})
    }

    const goToProvider = (Item) => {
      console.log('entre a creacion proveedor',Item)
      navigation.navigate('Proveedor',{register:Item})
    }

    const goToCaja =()=>{
      console.log('entre a cajas')
      navigation.navigate('Cajas',{})
    }

    const goToGasto = (Item) => {
      navigation.navigate('Gasto',{product:Item})
    }
    const goToPago = (Item) => {
      console.log('entre a Pago',Item)
      navigation.navigate('Pago',{product:Item})
      console.log('mande el gasto',Item)
    } 
    const goToFacturaCliente = () => {
      navigation.navigate('Facturas',{type_invoice:client})
    }
    const goToFacturaProveedor = () => {
      navigation.navigate('Facturas',{type_invoice:provider})
    }
    return (
      <View style={{height:'100%',backgroundColor:'#fff'}}>  
        <Text style={{fontWeight:'bold',fontSize:50,marginTop:'20%',alignSelf:'center',marginBottom:'10%',color:'#3A86FF'}}>Menu</Text>    
          <Button
           title="Crear Cliente"
           buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
           titleStyle={{ marginHorizontal: 10,color:'black'  }}
           containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center' }}
           icon={<Icon name="user-plus" type="font-awesome-5" size={25} color='black' />}
           onPress={()=>goToClient('cliente')}
          />
          <Button
           title="Cajas"
           buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
           titleStyle={{ marginHorizontal: 10, color:'black'  }}
           containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center' }}
           icon={<Icon name="th-large" type="font-awesome-5" size={25} color='black'  />}
           onPress={goToCaja}
          />
           <Button
           title="Gasto"
           buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
           titleStyle={{ marginHorizontal: 10, color: 'black' }}
           containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center' }}
           icon={<Icon name="shopping-cart" type="font-awesome-5" size={25} color='black' />}
           onPress={()=>goToGasto('gasto')}
          /> 
          <Button
           title="Pago"
           color='red' 
           buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
           titleStyle={{ marginHorizontal: 10, color: 'black' }}
           containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center' }}
           icon={<Icon name="money" type="font-awesome-5" size={25} color='black' />}
           onPress={()=>goToPago('Pago')}
          />
          <Button
           title="Factura Clientes"
           color='red' 
           buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
           titleStyle={{ marginHorizontal: 10, color: 'black' }}
           containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center' }}
           icon={<Icon name="tablet" type="font-awesome-5" size={25} color='black' />}
           onPress={goToFacturaCliente}
          />
          <Button
           title="Crear Proveedor"
           buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
           titleStyle={{ marginHorizontal: 10,color:'black'  }}
           containerStyle={{ width: '55%', margin: 0, padding: 5, alignSelf: 'center' }}
           icon={<Icon name="user-plus" type="font-awesome-5" size={25} color='black' />}
           onPress={()=>goToProvider('proveedor')}
          />
          <Button
           title="Factura Proveedor"
           color='red' 
           buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
           titleStyle={{ marginHorizontal: 10, color: 'black' }}
           containerStyle={{ width: '55%', margin: 0, padding: 5, alignSelf: 'center' }}
           icon={<Icon name="tablet" type="font-awesome-5" size={25} color='black' />}
           onPress={goToFacturaProveedor}
          />
      </View>

    )
  }
  
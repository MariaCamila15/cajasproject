import { View, Text, TouchableOpacity, ScrollView, Alert,ActivityIndicator} from 'react-native';
import React, { useState,useEffect } from 'react';
import {Button,Icon} from 'react-native-elements';
import {CloseBox,GetDetailBox, OpenBox} from '../../services/box.services.js';
import {useNavigation} from '@react-navigation/native';
import Modal from 'react-native-modal';


export default function BoxDetails(props) {
  console.log('dataBox en detalles',props.route.params) 
  const [dataBox,setDataBox] = useState(props.route.params.detalle) 
  const [modalType,SetModalType] = useState(false)
  const [detalles,setDetalles] = useState(<></>)
  const navigation = useNavigation();
  const [loading, setloading] = useState(false);

 
  const goToBox = () => {
    navigation.navigate('Cajas',{})
  }

  const CerrarCaja = async () => {
    Alert.alert('Esta caja ha sido cerrada')
    console.log('cerrar caja',dataBox)
    let data ={
        id:parseInt(dataBox.id)
    }
  await CloseBox(data,(flag,response)=>{
    if(flag){
      console.log("respuestaService CloseBox",response)
      goToBox()
    } 
   })
  }

  const AbrirCaja = async () => {
    Alert.alert('Esta caja ha sido abierta')
    console.log('abrir caja',dataBox)
    let data ={
      id:parseInt(dataBox.id)
    } 
  await OpenBox(data,(flag,response)=>{
      if(flag){
        console.log("respuestaService OpenBox",response)
        goToBox()
       /*  setDetalles(data)  */
      } 
    })
  } 

  useEffect(()=>{
    inicializar()
  },[])

  const inicializar =() => {
    obtenerDetalleCaja();
  }

  const obtenerDetalleCaja = async () => {
    setloading(true)
    let data ={
      id:parseInt(dataBox.id)
   }
    await GetDetailBox(data,(flag,response)=>{
      if(flag){
        console.log('obtener detalle de caja',response.caja_detalles)
        MostrarDetallesCajas(response.caja_detalles) 
      }
    })}

  const MostrarDetallesCajas = (response) => {
    let row=[]
    response.forEach(element => {
      console.log(element)
      row.reverse().push(
        <View  key={element.id} style={{width:'95%', backgroundColor:'#E5E5E5c0',paddingTop:'5%',borderColor:'#CED4DA',borderWidth:1}}>
          <View style={{flexDirection:'row',justifyContent:'space-around'}}>
            <Text style={{fontWeight: 'bold'}}>Id</Text>
            <Text style={{fontWeight: 'bold'}}>Tipo</Text>
            <Text style={{fontWeight: 'bold'}}>Hora Dispositivo</Text>
            <Text style={{fontWeight: 'bold'}}>Valor</Text>
          </View>
          <View style={{flexDirection:'row',justifyContent:'space-evenly', marginBottom:10}}>
            <Text>{element.id}</Text>
            <Text>{element.tipo}</Text>
            <Text>{element.hora_dispositivo}</Text>
            <Text>{element.valor}</Text>
          </View>
          <View style={{flexDirection:'row',marginLeft:5}}>
            <Text style={{fontWeight: 'bold'}}>Descripcion:</Text>
          </View>
          <View style={{flexDirection:'row',marginBottom:10,marginLeft:5}}>
            <Text>{element.descripcion}</Text>  
          </View>
        </View>
      )
    });
    setloading(false)
    setDetalles(row)
  } 

  const goToPago = (Item) => {
    console.log('entre a Pago',Item)
    navigation.navigate('crearPago',{product:Item})
    SetModalType(false)
  }

  const goToGasto = (Item) => {
    console.log('entre a Gasto',Item)
    navigation.navigate('Gasto',{product:Item})
    SetModalType(false)
  }
  
  return (
  <View style={{height:'100%'}}>
    <View style={{paddingTop:'5%',paddingLeft:'3%',backgroundColor:'#fff',height:'90%',width:'100%'}}>
      <View style={{flexDirection:'row'}}>
        <Text style={{fontWeight: 'bold'}}>Id:</Text>
        <Text>{dataBox.id}</Text>
      </View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly',margin:5}}>
        <Text style={{fontWeight: 'bold'}}>Fecha:</Text>
        <Text>{dataBox.fecha}</Text>
        <Text style={{fontWeight: 'bold'}}>Saldo Final:</Text>
        <Text>{dataBox.saldo_final}</Text>
      </View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly',margin:5}}>
        <Text style={{fontWeight: 'bold'}}>Estado:</Text>
        <Text>{dataBox.estado}</Text>
        <Text style={{fontWeight: 'bold'}}>Saldo Inicial:</Text>
        <Text>{dataBox.saldo_inicial}</Text> 
      </View>
      <View>
        <View style={{width:'95%',height:'10%', backgroundColor:'#CED4DA',marginTop:'15%',flexDirection:'row',justifyContent:'space-around',paddingTop:10}}>
          <Text style={{fontWeight: 'bold',fontSize:20,marginHorizontal:50,justifyContent:'center'}}>Detalle de cajas</Text>
          <Icon name="edit" type="font-awesome-5" size={30} color='black' onPress={()=>{SetModalType(true)}}/>
        </View>
        <Modal style={{flexDirection: 'column'}} isVisible={modalType}>
          <TouchableOpacity onPress={() => SetModalType(false)}>
            <View style={{backgroundColor:'#fff',width:'95%',height:'35%',alignSelf:'center',justifyContent:'center',paddingTop:'5%'}}>
              <View style={{flexDirection:'row'}}>
                <Button
                  title="Agregar detalle tipo Pago"
                  buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
                  titleStyle={{ marginHorizontal: 2, color:'black'  }}
                  containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center',height:'120%'}}
                  onPress={()=> goToPago('Pago')}
                />
                <Button
                  title="Agregar detalle tipo gasto"
                  buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
                  titleStyle={{ marginHorizontal: 5, color:'black'  }}
                  containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center',height:'120%' }}
                  onPress={()=> goToGasto('Gasto')}
                />
              </View>
            </View>
          </TouchableOpacity>
         </Modal>
        <ScrollView style={{width:'100%',alignSelf:'center',height:'60%'}}>
          {detalles}
        </ScrollView>      
        <ActivityIndicator
        animating={loading}
        color='#003049'
        size="large"
        style={{position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',}}
      />
      </View>
    </View>  
    <View style={{flexDirection:'row', justifyContent:'space-around',height:'10%',backgroundColor:'#ffff'}}>
      <Button
        title="Abrir Caja"
        buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
        titleStyle={{ marginHorizontal: 5, color:'black'  }}
        containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center' }}
        icon={<Icon name="chevron-up" type="font-awesome-5" size={25} color='black'  />}
        onPress={()=> AbrirCaja()}
      />
      <Button
        title="Cerrar Caja"
        buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
        titleStyle={{ marginHorizontal: 5, color:'black'  }}
        containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center' }}
        icon={<Icon name="chevron-down" type="font-awesome-5" size={25} color='black'  />}
        onPress={()=> CerrarCaja()}
      />   

    </View>

  </View>                
  )
}
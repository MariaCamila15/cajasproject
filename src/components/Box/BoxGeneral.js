import { View, Text, TouchableOpacity, ScrollView,ActivityIndicator,ToastAndroid} from 'react-native';
import React,{useState,useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import {CreateBox,OpenBox,ListBox} from '../../services/box.services.js';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function BoxGeneral() {
  const [dataBox,setDataBox] = useState([]);
  const [lista,setLista]=useState(<></>)
  const navigation = useNavigation();
  const [loading, setloading] = useState(false);

  const CrearCaja = async () => {
    ToastAndroid.show(
      'Se ha creado una caja nueva',
      ToastAndroid.LONG,
    );
    //solucionar lo de diario_id:7
     let data ={
      diario_id:7
    }  
  await CreateBox( data,(flag,response)=>{
    if(flag){
      setDataBox(response)
      console.log('response createBox',response)
       try{
        AbrirCaja(response) 
      }catch (error) {
        alert('error');
      } 
    } 
  })
  }

   const AbrirCaja = async () => {
    let data ={
      id: dataBox.id
    } 
  await OpenBox(data,(flag)=>{
      if(flag){
        setDataBox(data)
      } 
    })
  } 
/*  const AbrirCaja = async (id) => {
    let data ={
      id:parseInt(id)
    } 
  await OpenBox(data,(flag,response)=>{
      if(flag){
        setLista(data)
      } 
    })
  } 
  */
   useEffect(() => {
    inicializar();

  },[]) 

  const inicializar = () =>{
    ListarCajas();
  }
  
  const ListarCajas = async () => {
   setloading(true);
  await ListBox("",(flag,response)=>{
    if(flag){
      MostrarListaCajas(response.caja_ids) 
    }
  })}

const MostrarListaCajas = (response)=>{
  setloading(false);
    let row=[]
      response.forEach(element => {
       /*  console.log(element) */
        row.reverse().push(
          <TouchableOpacity key={element.id} onPress={()=>{navigation.navigate('DetalleCaja', {detalle:element})}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{fontWeight: 'bold'}}>Id:</Text>
              <Text>{element.id}</Text>
            </View>  
            <View style={{borderBottomWidth:1,marginBottom:10}}>
              <View style={{flexDirection: 'row',justifyContent:'space-evenly'}}>
                <Text style={{fontWeight: 'bold'}}>Fecha:</Text>
                <Text>{element.fecha}</Text>
                <Text style={{fontWeight: 'bold'}}>Saldo Final:</Text>
                <Text>{element.saldo_final}</Text>
              </View>
              <View style={{flexDirection: 'row',justifyContent:'space-evenly'}}>
                <Text style={{fontWeight: 'bold'}}>Estado:</Text>
                <Text>{element.estado}</Text>
                <Text style={{fontWeight: 'bold'}}>Saldo Inicial:</Text>
                <Text>{element.saldo_inicial}</Text> 
              </View>                
            </View>
          </TouchableOpacity>            
        )     
      });
      setLista(row)
  }    

  return (
    <View style={{backgroundColor:'#fff',height:'100%'}}>
      <Button
        title="Crear Cajas"
        buttonStyle={{backgroundColor:  'transparent',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
        titleStyle={{ marginHorizontal: 10, color:'black'  }}
        containerStyle={{ width: '38%', margin: 0, padding: 3 }}
        icon={<Icon name="stop" type="font-awesome-5" size={20} color='black'  />}
        onPress={()=> {CrearCaja()}} 
      />
      <View style={{height:'95%'}} >
      <ActivityIndicator
        animating={loading}
        color='#003049'
        size="large"
        style={{position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',}}
      />
        <Text style={{fontSize:25,fontWeight:'bold',backgroundColor:'#E5E5E5',width:'100%',textAlign:'center',height:'8%',paddingTop:'3%'}}>Lista de Cajas</Text>
         <ScrollView style={{width:' 100%',alignSelf:'center'}}>
           {lista}
         </ScrollView>         
      </View>
    </View>
  )
}

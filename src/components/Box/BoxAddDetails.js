import React,{useState,useEffect} from 'react'
import { View, 
  Text,
  TextInput, 
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
  Image} from 'react-native'
import {Picker} from '@react-native-picker/picker';
import {UploadTransaction} from '../../services/box.services'
import { launchCamera } from 'react-native-image-picker';
import {ReturnData} from '../../services/services';
import {Button,Icon} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native'
import reactiva from '../../utils/reactiva'

export default function BoxAddDetails(props) {
  console.log("TypeSpent",props)
    const navigation = useNavigation();
    const [descripcionProducto, setDescripcionProducto] = useState('');
    const [valorPrducto,setValorProducto] = useState('');
    const [fotoProducto,setFotoProduct] = useState();
    const [listarProduct,setListarProduct] = useState(<></>);
    const [selectType,setSelectType] = useState('');
    const [selectProduct,setSelectProduct] = useState('');
    const [selectClient,setSelectClient] = useState('');
    const [filePath,setFilePath]=useState('')
    
    
    const SaveToSpent = async() =>{
      if (valorPrducto.id  != null){
        alert("Debe ingresar una descripcion");
      }else{
        ToastAndroid.show("Guardando detalle producto", ToastAndroid.LONG);
         CargarTransaccion();
         navigation.navigate('DetalleCaja',{})
      }
    }

    const CargarTransaccion = async() => {
      if(selectType === 'gasto'){
      const data = [{
            id:-1, 
            detalle:{
                tipo: selectType,
                gasto_descripcion: descripcionProducto,
                gasto_foto_ids: [
                    [
                        0, 0, {
                            foto: filePath 
                        }
                    ]
                ],                
                gasto_concepto_id: parseInt(selectProduct),                 
                valor: parseInt(valorPrducto),
                hora_dispositivo: reactiva.formatoFecha(new Date())  
              }        
      }]
      try{
        const result= await UploadTransaction(data,(flag,response)=>{
          console.log('entro a subir transaccion',response)
          if(flag){
            console.log('resultado de subir transaccion',response)
          }
        })        
      }catch(error){
      alert(`Un error ocurrio grabando la evidencia: ${error.message}`);
     }}else{
      const data = [
        {
          id: -1,
          detalle: {
            tipo: selectType,
/*             gasto_foto_ids: [
              [
                0, 0, {
                  foto: filePath 
                }
              ]
            ], */
            pago_factura_venta_id: parseInt(selectVenta),
            valor: parseInt(valorPrducto),
            hora_dispositivo: reactiva.formatoFecha(new Date()),         
            venta_partner_id: false,
          }
        }]
      try{
        const result= await UploadTransaction(data,(flag,response)=>{
          console.log('entro a subir transaccion',response)
        if(flag){
          console.log('resultado de subir transaccion',response)
        }
      })        
      }catch(error){
        alert(`Un error ocurrio grabando la evidencia: ${error.message}`);
      }
     }
   }
    const takePicture = () =>{
      const options = {
        title: 'Tomar Foto',
        storageOptions: {
          skipBackup: true,
          path: 'images',
        }, 
        includeBase64: true,
      }     
      launchCamera(options, response=>{
        if(response.errorCode){
          console.log(response.errorMessage)
        }else if(response.didCancel){
          console.log('cancelo fotografia')
        }else {
          const uri = response.assets[0].uri
          setFotoProduct(uri); 
          setFilePath(response.assets[0].base64)
        }
      })
  }

  useEffect(() => {
    inicializar();

  },[]) 

  const inicializar =() => {
    RetornarProductos();
  }
  const RetornarProductos = async() => {
    await ReturnData("",(flag,response)=>{
      if(flag){
        MostrarProdutos(response.product)
      }
    })
  }

  const MostrarProdutos = (response) => {
    let row=[]
    response.forEach(element => {
      row.reverse().push(
        <Picker.Item key={'key'+element.name}value={element.id} label={element.name}/>        
    )
  }); 
    setListarProduct(row)
  } 

   return (
    <>
    <View style={{paddingTop:10,height:'100%',backgroundColor:'#fff'}}>
      <Text style={{alignSelf: 'center',fontWeight: 'bold',color:'#0582CA',fontSize:25,marginVertical:10}}>Agregar detalle</Text>
      <View style={{alignSelf:'center'}}>
        <Text style={styles.textLabel}>Seleccionar tipo de producto</Text>
        <View style={styles.picker}>
          <Picker
          selectedValue={selectType}
          onValueChange={(itemValue)=> setSelectType(itemValue)}>
            <Picker.Item label=' '/>
            <Picker.Item label='Gasto' value='gasto'/>
            <Picker.Item  label='Pago' value='pago'/>
          </Picker>
        </View>
      </View>
      <View style={{alignSelf:'center'}}>
        <Text style={styles.textLabel}>Seleccionar producto</Text>
        <View style={styles.picker}>
          <Picker
          selectedValue={selectProduct}
          onValueChange={(itemValue)=> setSelectProduct(itemValue)}>
            {listarProduct}
          </Picker>
        </View>
      </View>
      <View style={{alignSelf:'center'}}>
        <Text style={styles.textLabel}>Descripcion:</Text>
        <TextInput
        placeholder='Descripcion'
        multiline={true}
        numberOfLines={10}
        style={styles.textArea}
        value={descripcionProducto}
        onChangeText={text => {
            setDescripcionProducto(text)
        }}/>
      </View>
      <View style={{marginLeft:30}}>
        <Text style={styles.textLabel}>Valor:</Text>
        <TextInput
        placeholder='Valor'
        keyboardType = 'numeric'
        style={styles.textAreaValue}
        value={valorPrducto}
        onChangeText={number => {
          setValorProducto(number)
        }}/>
      </View>
        <Image
          style={{
          alignSelf:'center',
          height:'30%',
          width:'80%',
          backgroundColor:'#DEE2E6'
          }}
          source = {{uri: fotoProducto}}
        /> 
      <TouchableOpacity  style={styles.camera} onPress={ takePicture}>
        <Image source={require('../../assets/camara.png')}/>
      </TouchableOpacity>
      <Button
        title="Guardar"
        buttonStyle={{backgroundColor: '#E5E5E5',borderColor: 'transparent',borderWidth: 0,borderRadius: 5,}}
        titleStyle={{ marginHorizontal: 5, color:'black'  }}
        containerStyle={{ width: '50%', margin: 0, padding: 5, alignSelf: 'center' }}
        onPress={SaveToSpent}
      />
    </View>
    </>

  )
}

const styles= StyleSheet.create({
    picker:{
      borderColor:'black',
      borderWidth:1,
      width: 300,
      height: 50,
      marginBottom:10
    },
    textArea: {
      height: 80,
      width: 300,
      borderWidth: 1,
      borderRadius: 2,
      borderColor: 'black',
      marginBottom:10
    },
    textAreaValue: {
        height: 30,
        width: 150,
        borderWidth: 1,
        borderRadius: 2,
        borderColor: 'black',
        paddingVertical: 0,
        marginBottom:10
      },
    textLabel: {
        fontWeight: 'bold',
        fontSize: 13,
        marginLeft:5
      },
    guardar:{
      paddingLeft:2,
      borderColor:'black',
      borderWidth:1,
      height:30,
      borderRadius:5,
      width:100,
      alignSelf:'center'
    },
    camera:{
      marginLeft:'10%'
    }
  })
import  React from 'react';
import { View, StyleSheet} from 'react-native';
import PayList from './components/payList';

const Pay = (props) => {
    const {navigation} = props
return (
  <View>
    <View style={{backgroundColor: '#F2F2F2', width:'100%', marginTop: 10, borderRadius: 10, }}>
      <PayList navigation={navigation}/> 
    </View>
  </View>
    )
  }

  export default Pay
  const style = StyleSheet.create({
    container: {
        justifyContent: 'center',
         height: '100%',
         alignItems: 'center',
         width:'100%'
        },
  })
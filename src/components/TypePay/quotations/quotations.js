import  React from 'react';
import { View, StyleSheet, ImageBackground} from 'react-native';
import QuotationList from './components/quotationsList';
const Quotations = (props) => {
    const navigation= props.navigation
    const idLead = props.route.params.lead_id
    const partner_id = props.route.params.partner_id
   
   
return (
    <View>
        <ImageBackground  style = {style.container}  source={require('../../../assets/fondojorge2.png')}>
            <View style={{backgroundColor: '#F2F2F2', width:'100%', marginTop: 10, borderRadius: 10, }}>
                <QuotationList navigation={navigation} idLead={idLead} partner_id={partner_id}/>
            </View>
        </ImageBackground>
    </View>
    
    )
  }

  export default Quotations
  const style = StyleSheet.create({
    container: {
        justifyContent: 'center',
         height: '100%',
         alignItems: 'center',
         width:'100%'
        }
    }
  )
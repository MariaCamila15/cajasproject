
import  React, { useEffect, useState } from 'react';
import { SearchPicker } from '../../../../shared/picker/picker';
import { Input, Icon } from 'react-native-elements';
import {Alert, Switch,View, StyleSheet, ImageBackground,Image,Text, TouchableOpacity, FlatList, SafeAreaView, ToastAndroid } from 'react-native';
import { ReturnData, CreateQuotations } from '../../../../services/services';
import { FormatDateComplete } from '../../../../shared/inputs/formats';
import {useNavigation} from '@react-navigation/native'

export const CreateQuotation = (props) => {
    const navigation= useNavigation()
    const [products, setProducts] = useState([]);
    let [partner, setPartner] = useState(props.route.params.partner_id)
    const [leadId, setLeadId] = useState(props.route.params.lead_id);
    let [itemSelected, setItemSelected] = useState('');
    let [selectedProduct, setSelectedProduct]  = useState([]);

    const SendQuotation = () =>{
        
        let detalles=[]
        selectedProduct.forEach((element)=>{
            detalles.push({product_id:parseInt(element.value), price_unit: parseInt(element.price), name:element.label,product_uom_qty: element.amount, aplicaimpuesto: element.aplicaImpuesto })
        })
        const jsonenvio = {
            partner_id:parseInt(partner),
            opportunity_id: parseInt(leadId),
            validity_date: FormatDateComplete(Date.now()),
            detalles: detalles
        }
        CreateQuotations(jsonenvio,(flag,response)=>{
            if (flag){
                if(response.errores){
                    if(response.errores.error){
                        Alert.alert('¡Atención! ',response.errores.error);
                    }       
                }else{
                    ToastAndroid.show("Cotización Diligenciada correctamente", ToastAndroid.SHORT);
                    setSelectedProduct([])
                    navigation.navigate("Quotations",{lead_id:leadId}); 
                }
            }
        })
    }
    const deleteItem = (id) =>{
        const listOnDelete = selectedProduct.filter(product => product.value !== id)
        setSelectedProduct(listOnDelete)

    }
    useEffect(()=>{
        const product = [];
        const lead = [];
        const data={}
         ReturnData(data,(flag,response) =>{
            if(flag){
                response.product.forEach((element)=>{
                    if(element.venta===true){
                        product.push({label: element.name, value: element.id, price:0, amount: 0, aplicaImpuesto: false})
                    }
                    
                })
                product.sort((a,b) =>{
                    return (a.label < b.label) ? -1 : 1
                })
                setProducts(product)
                response.res_partner.forEach((partners)=>{
                    if (partners.name===partner){
                        setPartner(partners.id)
                    }
                })
                

            }
        })
    },[])

    return (
          <ImageBackground  style = {style.container} source={require('../../../../assets/fondojorge2.png')}>
            <TouchableOpacity onPress={()=>{props.navigation.navigate("MenuEmployee")}}>
                        <Icon name='arrow-back' color={'#fff'} size={40} style={{marginLeft:230}}/>
            </TouchableOpacity> 
                    <View style={style.body}>
                        <SearchPicker
                        style={{ button: { marginTop: 10 } }}
                        value={itemSelected}
                        items={products}
                        onChange={(product) => { selectedProduct.push(product);
                            setItemSelected(product);
                        }}
                        />
                        <SafeAreaView style={{height:350, marginTop:10, backgroundColor: 'rgba(255,255,255,0.2)'}}>
                            <FlatList style={{marginTop:20}}
                            data={selectedProduct}
                            scrollEnabled={true}
                            renderItem={({ item, key }) =>
                            <ItemQuotation items={item} key={key} deleteItem={deleteItem}/>
                            } 
                            />
                        </SafeAreaView>
                    </View>
                    <View style={style.buttonRU}>
                        <TouchableOpacity onPress={SendQuotation}>
                        <Text style={style.button}>Generar Cotización</Text>
                        </TouchableOpacity> 
                    </View>
          </ImageBackground>
        
      )
    }
export default CreateQuotation;
  
    const style = StyleSheet.create({
      container: {  
          alignSelf:'flex-start',
           height: '100%',
           width:'100%', 
          },  
      body:{
        alignSelf:'center',
        flexDirection:'column',
        paddingTop:50,
        marginHorizontal: 10,
        width: '80%',
      },
      profile:{
        flexDirection:'row',
        alignSelf:'center',
        width: '50%',
    },
      person:{
        padding: 7,
        width: '86%',
        fontSize: 17,
        fontFamily:'sans-serif-light',   
    },
      text:{
        textAlign:'center',
        color:'white',
        paddingTop:10,
        paddingBottom:10
      },
      buttonRU:{
        flexDirection:'row',
        backgroundColor: 'rgba(0,0,0,0.2)',
        alignSelf: 'center',
        marginLeft: 8,
        marginVertical: 26,
        width: '80%',
        height:'5%',
        borderRadius:4,
        justifyContent: 'center'
    },
      button:{ 
        fontSize: 20, 
        color:'#fff', 
        alignSelf:'flex-end',
        fontWeight: 'bold' 
      },
      
    })

const ItemQuotation =(props) => {
    const {items,deleteItem} = props
    const [isEnabled, setIsEnabled] = useState(false);
    const [lista,setLista]=useState(items)
    const toggleSwitch = (value) => {
        items.aplicaImpuesto= value
        setIsEnabled(previousState => !previousState);
    }
   
        return (
                <View style={{flexDirection: 'row', padding: 3, borderBottomWidth: 1, borderBottomColor: 'gray'}}>
                    <View style={{ borderRightWidth: 1, width: '33%', }}>
                        <Text style={{ fontSize: 15, textAlign: 'center' }}>{items.label}</Text>
                        <TouchableOpacity onPress={()=>{deleteItem(items.value)}} style={{marginTop:10}}>
                            <Icon name='close' color={'#ff0000'} size={40} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ borderRightWidth: 1, width: '33%', }}>
                        <Input style = {style.person}
                                    value={items.amount} 
                                    onChangeText = {(text) =>{items.amount=text}}
                                    placeholder="Cantidad" 
                                    keyboardType='numeric'
                                    placeholderTextColor={'rgba(255,255,255,0.5)'}      
                        /> 
                    </View> 
                    <View style={{ borderRightWidth: 1, width: '33%', }}>
                        <Input style = {style.person}
                                    value={items.price} 
                                    onChangeText = {(text) =>{items.price= text}}
                                    placeholder="Precio" 
                                    keyboardType='numeric'
                                    placeholderTextColor={'rgba(255,255,255,0.5)'}      
                        /> 
                        
                        <Text style={{ fontSize: 15, textAlign: 'center' }}>¿Impuesto?</Text>
                        <Switch
                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={toggleSwitch}
                            value={isEnabled}
                        />
                    </View> 
                </View>
        )
    }
    
        

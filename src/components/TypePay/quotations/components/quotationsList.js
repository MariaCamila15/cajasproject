import  React,{useState, useEffect} from 'react';
import { View,Text, StyleSheet,TouchableOpacity, ImageBackground, ScrollView, ToastAndroid, Alert} from 'react-native';
import { ListItem, Icon } from 'react-native-elements';
import { CreateInvoice, confirmQuotation, ListQuotations} from '../../../../services/services'
import  Modal  from 'react-native-modal';
import ModalPayQuotation from '../../../../utils/modals/modalPayQuotation'
import ModalRegisterPayment from '../../../../utils/modals/modalRegisterPayment';
import {ModalDetailQuotation} from '../../../../utils/modals/modalDetailQuotation'
import { useNavigation } from '@react-navigation/native';


const QuotationList = (props) => {
    const navigation = useNavigation()
    const {partner_id, idLead} = props
    const [quotations, setQuotations] = useState([])
    const [idQuotations, setIdQuotations] = useState('')
    const [invoice_id, setInvoice_id] = useState('');
    let [detail, setDetail] = useState(false);
    let [details, setDetails] = useState([]);
    const [commingLead, setCommingLead] = useState(false)
    const [pay, setPay] = useState(false)
    const [topay, setToPay] = useState(false)
    const [countPage, setCountPage] = useState(1)

    const createInvoice = (value) =>{
        let data = {
            cotizacion: parseInt(value)
        }
         CreateInvoice(data,(flag,response)=>{
            if(flag){
                ToastAndroid.show("Factura creada correctamente",ToastAndroid.SHORT)
                navigation.navigate("Menu")
            }else{
                Alert.alert("¡Atención!",response.data.arguments[0])
            }
        }) 
    }
    const callback = (flag,response) => {
        
        if(flag){
            ToastAndroid.show("Cargando cotizaciones...",ToastAndroid.SHORT)
            let cotizations=quotations
            if(response.cotizaciones_ids.length>0){
                response.cotizaciones_ids.forEach((element)=>{  
                    let id_invoice;
                    element.invoice_ids.forEach((factura)=>{
                        id_invoice = factura.id
                    })
                    if(element.state==="draft"){        
                        cotizations.push({label:element.name, details:element.detalles_ids, value: element.id,partner_id:element.partner_id,sale:false, invoice: id_invoice, draft:true, validity_date: element.validity_date})
                    }if(element.state==="sale"){
                        cotizations.push({label:element.name, details:element.detalles_ids, value: element.id,partner_id:element.partner_id,sale:true,invoice: id_invoice, draft:false, validity_date: element.validity_date})
                    } 
                })
                cotizations.sort((a,b) =>{
                    return (a.label > b.label) ? -1 : 1
                })
                setQuotations(cotizations)   
                setCountPage(countPage+1)
            }else{
                ToastAndroid.show("Se han cargado todas las cotizaciones", ToastAndroid.SHORT)
            }
            
        }  
    };
    const checkQuotation = async(value) => { 
        let data ={
            cotizacion: value
        }
        confirmQuotation(data,(flag,response)=>{
            if(flag){
                if(response.errores){
                    if(response.errores.error){
                        Alert.alert('¡Atención! ',response.errores.error);
                    }
                }else{
                    ToastAndroid.show("Cotización confirmada",ToastAndroid.SHORT)
                }
            }     
        })
    }

    const goBack = async() =>{
        navigation.navigate("Menu");setQuotations([]); setCountPage(1)
    }

    const loadQuotations =  async() =>{
        if(idLead===0){
            setCommingLead(false)
            data={
                pagina:countPage
            }
        }else{
            setCommingLead(true)
            data={
                opportunity_id:idLead,
                pagina:countPage
            }
        }
       
        ListQuotations(data,callback)
    }
        
    
    const reloadQuotations =  async() =>{
        if(idLead===0){
            setCommingLead(false)
            data={
                pagina:countPage
            }
        }else{
            setCommingLead(true)
            data={
                opportunity_id:idLead,
                pagina:countPage
            }
        }
       
        ListQuotations(data,callback)
    }

    useEffect(()=>{
        loadQuotations()
    },[idLead])

   
return (
  <View>
            <View style={{flexDirection:'row',width:'100%', marginTop:20}}>
                <View style={{ width:'70%', alignItems:'flex-start', marginLeft:15 }}>
                    <TouchableOpacity onPress={()=>{goBack()}}>
                        <Icon name='arrow-back' color={'#fff'} size={40}/>
                    </TouchableOpacity> 
                </View>
                <View style={{ width:'30%', flexDirection:'row', marginTop:0}}>
                    {commingLead &&<TouchableOpacity onPress={()=>{props.navigation.navigate("CreateQuotation",{lead_id: idLead, partner_id: partner_id})}}>
                        <Icon name='add-chart' color={'#fff'} size={40} />
                    </TouchableOpacity>}
                    <TouchableOpacity onPress={reloadQuotations}>
                        <Icon name='file-download' color={'#fff'} size={40} />
                    </TouchableOpacity>
                </View>
            </View>  
            <View style={style.body}>
            <ScrollView style={{width:'100%', marginTop:20, backgroundColor: 'rgba(255,255,255,0.2)'}}>
                {quotations.map((quotation) => {
                  return (
                    <ListItem
                    topDivider={true}
                    key={quotation.value}
                    bottomDivider
                    containerStyle={{backgroundColor:"transparent"}}
                    onPress={()=>{setDetail(true);setDetails(quotation.details)}}
                    >
                  <ListItem.Content >
                    <View style={{alignContent:'center', width:'100%'}}>
                        <View style={{flexDirection:'row', width:'100%', alignContent:'center'}}>
                            <Text style={{fontWeight:'bold', fontSize:18}}>Cotización: {quotation.label}</Text>
                        </View>
                        
                        <View style={{flexDirection:'row', marginTop:10, width:'100%'}}>
                            <View style={{ width:'33%'}}>
                                <Text style={{fontWeight:'bold'}}>Fecha:</Text>
                                <Text> {quotation.validity_date}</Text>
                            </View>
                            <View style={{ width:'33%'}}>
                                <Text style={{fontWeight:'bold'}}>Cliente:</Text>
                                <Text>{quotation.partner_id}</Text>
                            </View>
                            <View style={{ width:'34%'}}>
                                <Text style={{fontWeight:'bold'}}>Factura :</Text>
                                <Text>{quotation.invoice}</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row', width:'70%', marginTop:20}}>
                            <View style={{flexDirection:'row', width:'100%', marginTop:20}}>
                            <View style={{ width:'50%'}}>
                                {quotation.draft &&<Icon color={'green'}  name='check' size={35} onPress={()=>{checkQuotation(quotation.value)}} ></Icon>}
                                {quotation.sale && !quotation.invoice &&
                                <TouchableOpacity  onPress={()=>{createInvoice(quotation.value)}}> 
                                    <Icon name="request-quote"  size={45}/>
                                    <Text style={style.text}>Sin abono</Text>
                                </TouchableOpacity>}
                            </View>
                            <View style={{ width:'50%'}}>
                                {quotation.sale && !quotation.invoice &&
                                <TouchableOpacity onPress={()=>{setIdQuotations(quotation.value); setPay(true)}} >
                                    <Icon name="request-quote" size={45}/>
                                    <Text style={style.text}>Con abono</Text>
                                </TouchableOpacity>}  
                            </View>
                        </View>
                        
                      </View>
                    </View>
                  </ListItem.Content>
                </ListItem>
              );
            })}  
            <Modal
                children={""}
                style={{ flex: 1 }}
                isVisible={pay}
                onRequestClose={() => {
                  setPay(false);
                }}
                onBackButtonPress={() => {
                  setPay(false);
                }}
                onBackdropPress={() => {
                  setPay(false);
                }}
            >
              <ModalPayQuotation closeModal={()=>{setPay(false); goBack()}} quotation_id={idQuotations}/>
            </Modal>
            <Modal
                children={""}
                style={{ flex: 1 }}
                isVisible={topay}
                onRequestClose={() => {
                  setToPay(false);
                }}
                onBackButtonPress={() => {
                  setToPay(false);
                }}
                onBackdropPress={() => {
                  setToPay(false);
                }}
            >
              <ModalRegisterPayment closeModal={()=>{setToPay(false);goBack()}} invoice_id={invoice_id}/>
            </Modal>
            <Modal
                children={""}
                style={{ flex: 1 }}
                isVisible={detail}
                onRequestClose={() => {
                    setDetail(false);
                }}
                onBackButtonPress={() => {
                    setDetail(false);
                }}
                onBackdropPress={() => {
                    setDetail(false);
                }}
            >
                <ModalDetailQuotation details={details} closeModal={()=>{setDetail(false)}} />
              
            </Modal>         
          </ScrollView>
        </View>   
    </View>
    )
  }

  export default QuotationList
  const style = StyleSheet.create({
    container: {
        justifyContent: 'center',
         height: '100%',
         width:'100%'
        },
    body:{
            alignSelf:'center',
            flexDirection:'column',
            paddingTop:0,
            marginHorizontal: 10,
            width: '96%',
        },
    form: {
        borderRadius: 12,
        height: '78%',
        width: '80%',
        alignSelf:'center'
    },
    
    logo:{
        width: '100%',
        height: '100%'
    },
     header:{
        alignContent: 'center',
        width: '17%',
        height: '8%'
    },
    input:{
        flexDirection:'row',
        borderBottomColor: 'white',
        borderBottomWidth: 0,
        marginHorizontal: 28,
        marginTop: 8,
        width: '80%'
    },
    depto: {
        flexDirection:'row',
        borderBottomColor: 'white',
        borderBottomWidth: 0,
        marginLeft: 28,
        marginRight: 6,
        marginTop: 8,
        width: '65%' 
    },
    type:{
       flexDirection: 'row',
        borderBottomColor: 'white',
        borderBottomWidth: 0,
        marginRight:70,
        marginTop: 10,
        width: '37%'
    },
    select:{
        width:'75%',
        paddingLeft:7,
        paddingRight:0,
        backgroundColor:'transparent'
    },
    textSelect:{
        color:'rgba(255,255,255,0.5)',
        fontSize:17,
        fontFamily:'sans-serif-light',
    },
    despliegue:{
        width:'34%',
        height:'13%',
        alignSelf:'center',
        backgroundColor:'trasparent',
    },
    textDes:{
        color:'rgba(255,255,255,0.9)',
        fontSize:20,
        fontFamily:'sans-serif-light',
    },
    rowst:{
        backgroundColor:'rgba(251,245,153,0.6)',
        borderRadius:9,
        width:'82%'
    },
    input3:{
        flexDirection:'row',
        borderBottomColor: 'white',
        borderBottomWidth: 0,
        alignSelf:'center',
        marginTop: 10,
        width: '47%'
    },
    profile:{
        alignSelf:'center',
        width: '30%',
        height: '15%',
        marginBottom:0,
        marginTop: 15
    },
    icon:{
        marginRight: 2,
        marginTop: 13,
        width: 19.3,
        height: 19
    },
    iconDir:{
        marginRight: 2,
        marginTop: 10,
        width: 18,
        height: 23
    },
    doc:{
        flexDirection: 'row',
        width: '65%',
        marginTop: 5 
    },
    document:{
        flexDirection:'row',
        borderBottomColor: 'white',
        borderBottomWidth: 0,
        marginRight:10,
        marginLeft:30,
        marginTop: 8,
        width: '80%'
    },
    deptCity:{
        flexDirection: 'row',
        marginTop: 5,
        width: '70%',
    },
    person:{
        padding: 7,
        width: '86%',
        fontSize: 17,
        fontFamily:'sans-serif-light',   
    },
    buttonRU:{
        backgroundColor: 'rgba(0,0,0,0.2)',
        alignSelf: 'center',
        marginRight: 8,
        marginVertical: 26,
        width: '81%',
        height:'8%',
        borderRadius:4,
        justifyContent: 'center'
    },
    button:{
        color:('white'),
        alignSelf:'center',
        fontWeight: 'bold',
        fontSize: 17,
        fontFamily:'sans-serif-light'
    },
    dpi:{
        color:'white',
        marginTop:30,
        fontWeight:'bold',
        fontFamily:'sans-serif-light'
    },
    text:{
        color:'black',
        fontSize:12,
        alignSelf:'center',
        fontWeight:'bold'
      },
  })
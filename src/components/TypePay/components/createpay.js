import React,{useEffect, useState} from "react";
import { useFormik } from 'formik';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {Text, View, StyleSheet, ImageBackground, TouchableOpacity, Button, ToastAndroid,ScrollView } from "react-native";
import {  Input, Icon } from "react-native-elements";
import DatePicker from "react-native-date-picker";
import {  AirbnbRating } from "react-native-elements";
import { createLead, ReturnData } from "../../../services/services";
import {Picker} from '@react-native-picker/picker';
import { SearchPicker } from "../../../utils/picker/picker";
import { FormatDateComplete } from "../../../utils/inputs/formats";
import { useNavigation} from '@react-navigation/native'

const CreatePay = (props) => {
    const [date, setDate] = useState(new Date())
    const [projects, setProjects] = useState([])
    const [customers, setCustomers] = useState([])
    const [uid,setUid] = useState()
    const [rating,setRating] = useState('')
    const [open, setOpen] = useState(false)
    const navigation = useNavigation()
    
    const {values,handleSubmit,resetForm,errors, setFieldValue} = useFormik ({
        initialValues:{
            nombre:'',
            date_deadline:'',
            IngresosPlanificados:'',
            partner_id:0,
            project_id:0,
            planned_revenue:0,
            descripcion:'',
        },
        onSubmit: values => {
            resetForm();
            const data = {
                name:values.nombre,
                date_deadline:FormatDateComplete(date),
                planned_revenue:parseInt(values.IngresosPlanificados),
                partner_id: parseInt(values.partner_id.value),
                project_id:parseInt(values.project_id),
                description:values.descripcion,
                user_id: uid,
                priority: rating
            }
            createLead(data,(flag,response)=>{
               if(flag){
                ToastAndroid.show("Datos diligenciados correctamente", ToastAndroid.SHORT);
                navigation.navigate('Menu',{})   
               }

            })
        },
        validate: values =>{
            const errors={}
            if(values.IngresosPlanificados<=0) errors.IngresosPlanificados="El costo debe ser mayor a cero"
            if(values.partner_id===0) errors.partner_id="Debe seleccionar un cliente"
            if(values.project_id===0) errors.project_id="Debe seleccionar un proyecto asociado"
            if(values.nombre.length===0) errors.nombre="Campo obligatorio"
            if(values.descripcion.length===0) errors.descripcion="Campo obligatorio"
            return errors
        } 
    })
    const callback = (flag,response) => {
        let row=[]
        let clients=[]
        if(flag){
            row.reverse().push({ label: "Seleccione una opcion", value: 0 })
            response.project.forEach(element => {
                row.reverse().push({ label: element.name, value: element.id })
            });
           setProjects(row)
           clients.push({ label: "Seleccione una opcion", value: 0 })
           response.res_partner.forEach(element =>{
            if(element.customer_rank>0){
                clients.push({ label: element.name, value: element.id })
            }
           })
           setCustomers(clients)
        }
        
        
    };
    const getId = async () =>{
        let user_id = JSON.parse(await AsyncStorage.getItem('uid'))
        setUid(user_id)
    }
    useEffect(() => {
        getId();
        const data={}
        ReturnData(data,callback)
    }, [])
    
    return(
       <>
            <ScrollView> 
                <View style={style.form }>
                    <Input style = {style.person}
                        onChangeText = {text => setFieldValue('nombre', text)} 
                        placeholderTextColor={'gray'} 
                        placeholder="Nombre del pago"
                        errorMessage={errors.nombre}
                        value={values.nombre}>
                    </Input>
                    <Input style = {style.person}
                        onChangeText = {text => setFieldValue('IngresosPlanificados', text)}
                        placeholderTextColor={'gray'} 
                        placeholder="Costo"
                        errorMessage={errors.IngresosPlanificados}
                        keyboardType="numeric"
                        value={values.IngresosPlanificados}>

                    </Input>
                    <Text style={style.text}>Proyecto Asociado</Text>       
                    <Picker
                        containerStyle={{
                        backgroundColor: 'black',
                        marginHorizontal: 10,
                        borderColor: 'black',
                        borderRadius: 10,
                        }}
                        selectedValue={values.project_id}
                        style={{ height: 60, }}
                        onValueChange={(itemValue, itemIndex) => {
                        setFieldValue('project_id', itemValue)
                        }
                        }>
                            {
                                projects.map((element, index) => {
                                return <Picker.Item key={'key' + element.value} label={element.label} value={element.value} />
                                }) 
                            }
                        </Picker>
                        <Text style={style.textError}>{errors.project_id}</Text> 
                        <Text style={style.text}>Cliente solicitante</Text>       
                        <SearchPicker
                            style={{ button: { marginHorizontal: 0 } }}
                            value={values.partner_id}
                            items={customers}
                            onChange={(itemValue, itemIndex) => {
                                setFieldValue('partner_id', itemValue)}}
                        /> 
                        <Text style={style.textError}>{errors.partner_id}</Text> 
                        
                        <Input style = {style.person}
                            onChangeText = {text => setFieldValue('descripcion', text)} 
                            placeholderTextColor={'gray'} 
                            placeholder="Descripción"
                            multiline={true}
                            errorMessage={errors.descripcion}
                            value={values.descripcion}>
                        </Input>
                        <Button color={'#023047'} title={FormatDateComplete(date)} onPress={() => setOpen(true)} />
                        <DatePicker
                            modal
                            mode='date'
                            open={open}
                            date={date}
                            onConfirm={(date) => {
                            setOpen(false)
                            setDate(date)
                            }}
                            onCancel={() => {
                            setOpen(false)
                            }}
                        />
                        <View style={style.buttonRU}>
                            <TouchableOpacity onPress={handleSubmit}>
                                <Text style={style.button}>REGISTRAR OPORTUNIDAD</Text>
                            </TouchableOpacity>
                        </View>
                </View>
            </ScrollView>
       </> 
    )
}

export default  CreatePay
const style = StyleSheet.create({

    container:{
        justifyContent: 'center',
        height: '100%',
        alignItems: 'center',
        width:'100%',
        
        //backgroundColor:'blue'
    },
    

    form:{
        borderRadius: 12,
        marginTop:35,
        marginLeft:15,
        height: '100%',
        width: '90%',
        justifyContent:'center',
    },
    logo:{
        width: '100%',
        height: '100%'
    },
     header:{
        marginVertical:-15,
        marginBottom:10,
        alignContent: 'center',
        width: '17%',
        height: '8%'
    },
    person:{
        backgroundColor: 'transparent',
        borderRadius: 10,
        width: '100%',
        marginHorizontal: 12,
        borderRadius: 10,
        borderColor: 'black',
        height: 60,
        marginTop: 3,
    },    
    buttonRU:{
        backgroundColor: 'rgba(0,0,0,0.2)',
        alignSelf: 'center',
        marginRight: 8,
        marginVertical: 26,
        width: '81%',
        height:'8%',
        borderRadius:4,
        justifyContent: 'center'
    },
    button:{
        color:('black'),
        alignSelf:'center',
        fontWeight: 'bold',
        fontSize: 17,
        fontFamily:'sans-serif-light'
    },
    text:{
        color:('black'),
        alignSelf:'flex-start',
        fontWeight: 'bold',
        fontSize: 17,
        fontFamily:'sans-serif-light'
    },
    textError:{
        color:'#ff0000',
        alignSelf:'flex-start',
        fontSize: 12,
        fontFamily:'sans-serif-light'
    },
    type:{
        flexDirection: 'row',
         borderBottomWidth: 0,
         marginRight:70,
         width: '50%'
     },
    select:{
        width:'85%',
        paddingLeft:7,
        paddingRight:0,
        backgroundColor:'transparent'
    },
    arrow:{
        alignSelf:'center',
        marginLeft:-25
    },
    textSelect:{
        color:'rgba(255,255,255,0.5)',
        fontSize:17,
        fontFamily:'sans-serif-light',
    },
    despliegue:{
        width:'54%',
        height:'13%',
        alignSelf:'center',
        backgroundColor:'trasparent',
    },
    textDes:{
        color:'rgba(255,255,255,0.9)',
        fontSize:20,
        fontFamily:'sans-serif-light',
    },
    rowst:{
        backgroundColor:'rgba(251,245,153,0.6)',
        borderRadius:9,
        width:'82%'
    },
    star:{
        height:30,
        width:30,
       marginHorizontal: -9.5
    },
    boxCont:{
        flexDirection:"row",
        marginTop:-5,
        marginVertical:2
    },
    Textbox:{
        fontSize:17,
        marginTop:15,
        marginLeft:15,
        marginRight:17,
        color:'rgba(255,255,255,1)'
    },
    box:{
        height:'50%',
        width:'40%'
    },
    starText:{
        flexDirection:"row",
        alignSelf:"center",     
    },
    low:{
        marginRight:1,
        marginLeft:60,
        alignSelf:'center',
        color:'rgba(255,255,255,1)'
    },
    medium:{
        marginRight:10,
        marginLeft:15, 
        alignSelf:'center',
        color:'rgba(255,255,255,1)'
    },
    high:{
        marginRight:20,
        marginLeft:10,
        alignSelf:'center',
        color:'rgba(255,255,255,1)'
    },
})

 
import  React,{useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { View, StyleSheet, ImageBackground,Text, TouchableOpacity, ScrollView, ToastAndroid} from 'react-native';
import Modal from 'react-native-modal';
import {useNavigation} from '@react-navigation/native'
import { ListItem, Icon, Button } from 'react-native-elements';
import { ListLeads, ReturnData } from '../../../services/services';
import { AirbnbRating } from 'react-native-elements';
import { FormatMoney } from '../../../utils/inputs/formats';
import { SearchBar} from '../../../utils/picker/components/searchBar';
import ModalInitLead from '../../../utils/modals/modalInitLead';
import ModalEditLead from '../../../utils/modals/modalEditLead';


const PayList = (props) => {
  const navigation = useNavigation();
  const { user_id} = props
  const [leads, setLeads] = useState([])
  const [invoice,setInvoice] = useState([])
  const [list,setList] = useState([])
  const [sendTime, setSendTime] = useState(false)
  const [countPage, setCountPage] = useState(1)
  let [search, setSearch] = useState("");
  const [editLead, setEditLead] = useState(false)
  const [idLead,setLeadId]  = useState("")
  let [stages, setStages] = useState([])
  const [stage_id, setStage_id] = useState("")
  const [projectId,setPojectId]  = useState("")
  
  
  const callback = async(flag,response) => {
    ToastAndroid.show("Cargando pagos...",ToastAndroid.SHORT)
    if(flag){
      let oportunities=[]
      let stages = JSON.parse(await AsyncStorage.getItem("stages"))
      let stageName;
      if (response.leads_ids.length>0){
        response.leads_ids.forEach((element)=>{
          stages.forEach((stage)=>{
            if(parseInt(stage.value)===element.stage_id){
              stageName = stage.label
            }
          })  
          if(element.show_time_control==="start"){
            oportunities.push({label:element.name, value: element.id,namestage: stageName, priority: element.priority,partner_id:element.partner_id,proyect_id:element.proyect_id, planned_revenue: element.planned_revenue, show_time_control:false, stage: element.stage_id})
          }else{
            oportunities.push({label:element.name, value: element.id, namestage: stageName, priority: element.priority,partner_id:element.partner_id,proyect_id:element.proyect_id, planned_revenue: element.planned_revenue, show_time_control:true, stage: element.stage_id})
          }
        })
        oportunities.sort((a,b) =>{
          return  b.value-a.value
        })
        setLeads(oportunities)
      }
    }else{
      ToastAndroid.show("todas las oportunidades se han bajado", ToastAndroid.SHORT)
    }
  };
  const reloadLeads = async()=>{
    setCountPage(countPage+1)
    const data={
      pagina:countPage,
      user_id: user_id
    }
    ListLeads(data,callback);
  }
  const loadLeads = async() =>{
    setCountPage(1);
    setLeads([])
    let user_id = JSON.parse(await AsyncStorage.getItem('uid'))
    const data={
      pagina:countPage,
      user_id: user_id
    }
    await ListLeads(data,callback);
  }
  const loadingData = async ()  =>{
   await ReturnData("",async(flag,response)=>{
      if(flag){
        let stage=[]
        response.crm_stage.forEach((element)=>{
          stage.push({label: element.name, value: element.id})
        })
        setStages(stage)
        
        await AsyncStorage.setItem("stages",JSON.stringify(stage))
      }
    });
    await loadLeads();
    
  }
  const searchItems = (searchedValue) => {
    let newList = [];
    if (searchedValue === "") {
        setSearch(searchedValue);
        setInvoice(list)
        return
    }else{
      invoice.forEach(element => {    
        if ((element.partner).toLowerCase().includes(searchedValue.toLowerCase())) {
          newList.push(element);
        }
      });
      setSearch(searchedValue);
      setInvoice(newList);
    }
  }
  useEffect(() => {
    loadingData();
    
  }, [navigation])  
   
  const goToCreatePay =()=>{
    navigation.navigate("crearPago",{})
  }

  const goToQuotations = (lead)=> {
    navigation.navigate("Quotations",{lead_id:lead.value, partner_id: lead.partner_id})
  }

return (
  <View>
    <View style={{flexDirection:'row',width:'100%'}}>
      <View style={{ width:'30%', flexDirection:'row', marginLeft:'70%',}}>
        <TouchableOpacity onPress={goToCreatePay}>
          <Icon name='add-chart' color={'black'} size={40} />
        </TouchableOpacity>
        <TouchableOpacity onPress={reloadLeads}>
           <Icon name='file-download' color={'black'} size={40}/>
        </TouchableOpacity>
      </View>
    </View> 
    <View style={style.body}>
       <View style={{ width: '100%' }}>
        <SearchBar
          styleContainer={{ borderWidth: 1, marginTop: 5 }}
          icon="search"
          placeholder='Buscar...'
          action={() => { null }}
          value={search}
          onChangeText={(value) => searchItems(value)} 
        /> 
      </View>
      <ScrollView style={{width:'100%', marginTop:20, backgroundColor: 'rgba(255,255,255,0.2)'}}>
                {leads.map((lead, index) => {
                    return (
                    <ListItem
                        topDivider={true}
                        key={lead.value}
                        bottomDivider
                        containerStyle={{backgroundColor:"transparent"}}
                        onPress={() => goToQuotations}>
                    <ListItem.Content>
                    <View style={{alignContent:'center', width:'100%'}}>
                      <View style={{flexDirection:'row', width:'100%', alignContent:'center'}}>
                        <Text style={{fontWeight:'bold', fontSize:18}}>Oportunidad :{lead.value}-{lead.label}</Text>
                      </View>
                      <View style={{flexDirection:'row', marginTop:10, width:'100%'}}>
                        <View style={{ width:'50%'}}>
                          <Text>Cliente: {lead.partner_id}</Text>
                        </View>
                        <View style={{ width:'50%'}}>
                          <Text>Etapa: {lead.namestage}</Text>
                          <Text style={{fontWeight:'bold'}}>Planificación: {FormatMoney(lead.planned_revenue)}</Text>
                        </View>
                      </View>
                      <View style={{flexDirection:'row', width:'70%', marginTop:20}}>
                        <View style={{flexDirection:'row', width:'70%', marginTop:20}}>
                          <View style={{ width:'20%'}}>
                            {!lead.show_time_control &&<Icon color={'#023047'}  name='play-circle-filled' size={30} onPress={()=>{setSendTime(true);setLeadId(lead.value);setPojectId(lead.proyect_id)}}></Icon>}
                            {lead.show_time_control &&<Icon color={'red'}  name='pause' size={30} onPress={()=>{setSendTime(true);setLeadId(lead.value);setPojectId(lead.proyect_id)}}></Icon>}
                          </View>
                          <View style={{ marginLeft:20,width:'20%'}}>
                            <Icon color={'black'}  name='edit' size={30} onPress={()=>{setEditLead(true);setStage_id(lead.stage);setLeadId(lead.value)}}></Icon> 
                          </View>

                        </View>
                      </View>
                    </View>
                    </ListItem.Content>
                    </ListItem> 
                );
                })}
                <Modal
                    children={""}
                    style={{ flex: 1 }}
                    isVisible={sendTime}
                    onRequestClose={() => {
                    setSendTime(false);
                    }}
                    onBackButtonPress={() => {
                    setSendTime(false);
                    }}
                    onBackdropPress={() => {
                    setSendTime(false);
                    }}
                >
                <ModalInitLead closeModal={()=>{setSendTime(false);loadLeads()}} lead_id={idLead} project_id={projectId}/>
                </Modal>
                <Modal
                    children={""}
                    style={{ flex: 1 }}
                    isVisible={editLead}
                    onRequestClose={() => {
                    setEditLead(false);
                    }}
                    onBackButtonPress={() => {
                    setEditLead(false);
                    }}
                    onBackdropPress={() => {
                    setEditLead(false);
                    }}
                >
                    <ModalEditLead  stage_id={stage_id} lead_id={idLead} stages={stages} closeModal={()=>{setEditLead(false);loadLeads()}} />
                </Modal>           
            </ScrollView>
        </View>   
    </View>
    )
  }

export default PayList
  const style = StyleSheet.create({
    container: {
        justifyContent: 'center',
         height: '100%',
         width:'100%'
        },
    body:{
            alignSelf:'center',
            flexDirection:'column',
            paddingTop:0,
            marginHorizontal: 10,
            width: '96%',
        },
    form: {
        borderRadius: 12,
        height: '78%',
        width: '80%',
        alignSelf:'center'
    },
    
    logo:{
        width: '100%',
        height: '100%'
    },
     header:{
        alignContent: 'center',
        width: '17%',
        height: '8%'
    },
    input:{
        flexDirection:'row',
        borderBottomColor: 'black',
        borderBottomWidth: 0,
        marginHorizontal: 28,
        marginTop: 8,
        width: '80%'
    },
    depto: {
        flexDirection:'row',
        borderBottomColor: 'black',
        borderBottomWidth: 0,
        marginLeft: 28,
        marginRight: 6,
        marginTop: 8,
        width: '65%' 
    },
    type:{
       flexDirection: 'row',
        borderBottomColor: 'black',
        borderBottomWidth: 0,
        marginRight:70,
        marginTop: 10,
        width: '37%'
    },
    select:{
        width:'75%',
        paddingLeft:7,
        paddingRight:0,
        backgroundColor:'transparent'
    },
    arrow:{
        alignSelf:'center'
    },
    textSelect:{
        color:'rgba(255,255,255,0.5)',
        fontSize:17,
        fontFamily:'sans-serif-light',
    },
    despliegue:{
        width:'34%',
        height:'13%',
        alignSelf:'center',
        backgroundColor:'trasparent',
    },
    textDes:{
        color:'rgba(255,255,255,0.9)',
        fontSize:20,
        fontFamily:'sans-serif-light',
    },
    rowst:{
        backgroundColor:'rgba(251,245,153,0.6)',
        borderRadius:9,
        width:'82%'
    },
    input3:{
        flexDirection:'row',
        borderBottomColor: 'black',
        borderBottomWidth: 0,
        alignSelf:'center',
        marginTop: 10,
        width: '47%'
    },
    profile:{
        alignSelf:'center',
        width: '30%',
        height: '15%',
        marginBottom:0,
        marginTop: 15
    },
    icon:{
        marginRight: 2,
        marginTop: 13,
        width: 19.3,
        height: 19
    },
    iconDir:{
        marginRight: 2,
        marginTop: 10,
        width: 18,
        height: 23
    },
    doc:{
        flexDirection: 'row',
        width: '65%',
        marginTop: 5 
    },
    document:{
        flexDirection:'row',
        borderBottomColor: 'black',
        borderBottomWidth: 0,
        marginRight:10,
        marginLeft:30,
        marginTop: 8,
        width: '80%'
    },
    deptCity:{
        flexDirection: 'row',
        marginTop: 5,
        width: '70%',
    },
    person:{
        padding: 7,
        width: '86%',
        fontSize: 17,
        fontFamily:'sans-serif-light',   
    },
    buttonRU:{
        backgroundColor: 'rgba(0,0,0,0.2)',
        alignSelf: 'center',
        marginRight: 8,
        marginVertical: 26,
        width: '81%',
        height:'8%',
        borderRadius:4,
        justifyContent: 'center'
    },
    button:{
        color:('black'),
        alignSelf:'center',
        fontWeight: 'bold',
        fontSize: 17,
        fontFamily:'sans-serif-light'
    },
    dpi:{
        color:'black',
        marginTop:30,
        fontWeight:'bold',
        fontFamily:'sans-serif-light'
    }
  })
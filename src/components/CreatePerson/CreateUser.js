import  React,{useState, useEffect} from 'react';
import { View,
  ActivityIndicator, 
  StyleSheet, ImageBackground,
  ScrollView,
  Text, 
  TouchableOpacity, 
  Alert} from 'react-native';
import { Input } from 'react-native-elements';
import { useFormik} from 'formik';
import { createResPartner } from '../../services/services';

const CreateUser = (props) => {
  let [loading,setLoading] = useState(false)

  const callback = (flag, response) => {
    if (flag) {
        setLoading(false)
  } }

  const {values,setFieldValue, handleSubmit, errors, resetForm} = useFormik({
        
    initialValues:{
        nombre:'',
        direccion:'',
        celular:'',
        email:'',
    },
    onSubmit: values => {
        setLoading(true)
        resetForm();
        const data = {
            user_id_parent_id:1,
            is_company: false,
            name: values.nombre,
            street: values.direccion,
            l10n_co_document_type:'national_citizen_id',
            vat: values.documento,
            mobile: values.celular,
            email: values.email,
            city: 20,
            state_id: 652,
            country_id: 49,
        }
        console.log("datos que se van",data)
        
        createResPartner(data,callback)
       
    },
     validate:values =>{
      const errors={}
      if(values.nombre.length === 0) errors.nombre="Debe diligenciar el nombre"
      if(values.direccion.length === 0) errors.direccion="Debe diligenciar la direccion"
      if(values.celular.length !==10) errors.celular="Numero celular inválido"
      if(values.email.length === 0) errors.email="Debe diligenciar el correo"
      return errors
    } 
})

return (
  <View>      
        <ScrollView>
        <Text style={style.title}>Crear Usuario</Text>
        <View style={style.form }>
            <View style={style.input}>
                <Input style = {style.person} 
                           value={values.nombre}
                           onChangeText = {text => setFieldValue('nombre', text)} 
                           placeholder="Nombre"                               
                />          
            </View>
            <Text style={{color:'red', alignSelf:'center'}}>{errors.nombre}</Text>
            <View style={style.input}>
                <Input style = {style.person}
                           value={values.direccion}
                           onChangeText = {text => setFieldValue('direccion', text)}
                           placeholder="Dirección"                                
                />
            </View>
            <Text style={{color:'red', alignSelf:'center'}}>{errors.direccion}</Text>
            <View style={style.input}>
               <Input style = {style.person}
                        value={values.documento}
                        keyboardType={'numeric'}
                        onChangeText = {text => setFieldValue('documento', text)}
                        placeholder="Digite su cedula"                     
                />
            </View> 
            <Text style={{color:'red', alignSelf:'center'}}>{errors.documento}</Text>
            <View style={style.input}>
                <Input style = {style.person}
                           value={values.celular} 
                           onChangeText = {text => setFieldValue('celular', text)}
                           placeholder="Celular" 
                           keyboardType='numeric'   
                />
            </View>
            <Text style={{color:'red', alignSelf:'center'}}>{errors.celular}</Text>
            <View style={style.input}>
                <Input style = {style.person}
                           value={values.email}
                           onChangeText = {text => setFieldValue('email', text)}
                           placeholder="Email"
                />
            </View>
            <Text style={{color:'red', alignSelf:'center'}}>{errors.email}</Text>
            <View style={style.input}>
                <Input style = {style.person}
                           value = {values.pais}
                           onChangeText ={text => setFieldValue('pais', text)}
                           placeholder="Pais" 
                />
            </View>
            <Text style={{color:'red', alignSelf:'center'}}>{errors.pais}</Text>
            <View>
                <View style = {style.deptCity}>
                    <View style={style.depto}>
                        <Input style = {style.person}
                                   value = {values.departamento} 
                                   onChange = {(text) => setFieldValue('departamento', text)}
                                   placeholder="Departamento"        
                        />
                    </View>
                    <View style={style.input3}>
                        <Input style = {style.person}
                                   value = {values.ciudad}
                                   onChange = {text => setFieldValue('ciudad', text)}
                                   placeholder="Ciudad"        
                        />
                    </View>
                </View>
                <Text style={{color:'red', alignSelf:'flex-start'}}>{errors.departamento}</Text>
            </View>
            <Text style={{color:'red', alignSelf:'flex-end'}}>{errors.ciudad}</Text>
            <View style={style.buttonRU}>
                <TouchableOpacity onPress={handleSubmit}>
                    <Text style={style.button}>REGISTRAR USUARIO</Text>
                </TouchableOpacity>
            </View>
            <ActivityIndicator style={style.loading} collapsable={false} animating={loading} size={100} color="#8d1979"></ActivityIndicator>
        </View>
        </ScrollView>
  </View>
)
}
export default CreateUser;
const style = StyleSheet.create({
  container: {
      justifyContent: 'center',
       height: '100%',
       width:'100%'
      },
  title:{
      alignSelf: 'center',
      color:'#3A86FF',
      fontWeight: 'bold',
      fontSize:25,
      marginVertical:10

  },
  form: {
      height: '60%',
      width: '90%',
      alignSelf:'center'
  },
  loading:{
      position: 'absolute',
      marginLeft:80,
      marginTop:300,
  },
  depto: {
      flexDirection:'row',
      borderBottomColor: 'white',
      borderBottomWidth: 0,
      marginLeft: 5,
      marginRight: 6,
      marginTop: 3,
      width: '80%' 
  },
  input:{
      flexDirection:'row',
      alignSelf:'center',
      width: '100%'
  },
  input3:{
      flexDirection:'row',
      borderBottomWidth: 0,
      alignSelf:'center',
      marginTop: 3,
      width: '60%'
  },
  deptCity:{
      flexDirection: 'row',
      marginTop: 5,
      width: '70%',
  },
  person:{
      padding: 2,
      width: '90%',
      fontSize: 15, 
  },
   buttonRU:{
      backgroundColor: '#3A86FF',
      alignSelf: 'center',
      width: '80%',
      height:'10%',
      borderRadius:4,
      justifyContent: 'center',
      marginBottom:70,
  }, 
  button:{
      color:('white'),
      alignSelf:'center',
      fontWeight: 'bold',
      fontSize: 17,
      fontFamily:'sans-serif-light'
  },
})
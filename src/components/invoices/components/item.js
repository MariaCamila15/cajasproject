
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Alert } from "react-native";
import { Badge, Button, Icon } from 'react-native-elements';
import { FormatMoney } from '../../../utils/formats';
export const HistoryItem = (props) => {
    const { openModalPay, item, navigation, openModalDetail } = props;

    const [color, setColor] = useState('gray')

    return (
      <View>
        <View style={{ paddingRight: 8 }}>
          <Text style={{ fontWeight: 'bold', textAlign: 'right' }}>{item.date_invoice}</Text>
        </View>
        <View style={styles.row} >
          <TouchableOpacity onPress={() => openModalDetail()} style={{ width: '68%' }}>                
            <View style={{ flexDirection: 'row', width: item.state !== 'posted' ? '100%' : '70%' }}>
              <Icon name="person"  size={15} containerStyle={{ marginLeft: 0 }} color={color} raised ></Icon>
              <View style={{ justifyContent: 'center' }}>
                <Text style={[styles.label,]}>
                  {item.partner}
                </Text>
              </View>
              <View style={{ justifyContent: 'center', marginLeft: 20 }}>
                <Text style={{ fontWeight: 'bold' }}>
                  {FormatMoney(item.balance)}     
                </Text>
              </View>
            </View>
            <View>
              <Text style={styles.label}><Text style={{ fontWeight: 'bold' }}>Id Factura </Text>{item.id}</Text>
              <Text style={styles.label}><Text style={{ fontWeight: 'bold' }}>Fecha Factura: </Text>{item.date_invoice}</Text>
              <Text style={styles.label}><Text style={{ fontWeight: 'bold' }}>Saldo: </Text>{FormatMoney(item.balance)}</Text>
            </View>
          </TouchableOpacity>
            {item.state === 'posted' && <View style={{ width: '12%', }}>
              <Button
                onPress={() => openModalPay(item)}
                type="clear"
                buttonStyle={{ borderColor: 'gray' }}
                icon={
                <Icon name="edit"
                size={25}
                color='black'
                />}
              />
            </View>}
            {item.state !== 'posted' && <View style={{ width: '12%', }}></View>}
            {item.state !== 'posted' && <View style={{ width: '20%', marginVertical: 2, borderLeftWidth: 1, borderLeftColor: 'gray' }}>
              <Button
              onPress={() => navigation.navigate("Dues", { cuotas: item.factura })}
              type="clear"
              buttonStyle={{ borderColor: 'gray' }}
              icon={
                <Icon name="file-copy"
                size={25}
                color='gray'
                />}
              />
          </View>}
        </View>
      </View>
    );
}

var styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        borderBottomColor: "#BEBEBE",
        borderBottomWidth: 1,
        paddingVertical: 2,
        alignItems: 'center',
        justifyContent: 'center',

    },
    label: {
        fontSize: 14
    },
    titleHeader: {
        marginHorizontal: 12,
        fontSize: 14,
        fontWeight: "bold",
        marginTop: 4,
        marginBottom: 5
    },
    input: {
        fontSize: 20,
    },
});

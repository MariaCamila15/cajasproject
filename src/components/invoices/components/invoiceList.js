import React, { useEffect, useState } from 'react';
import { View, FlatList, StyleSheet, Text , ToastAndroid,ActivityIndicator} from "react-native";
import { useNavigation} from '@react-navigation/native'
import SearchBar from 'react-native-elements/dist/searchbar/SearchBar-android';
import { HistoryItem } from './item';
import { ListInvoice} from '../../../services/services'
import { Button, Icon } from 'react-native-elements'
import Modal from "react-native-modal";
import ModalRegisterPayment from '../../../utils/modals/modalRegisterPayment';
import {ModalDetailInvoice}  from '../../../utils/modals/modalDetailInvoice';
import { loadPartialConfig } from '@babel/core';



export const InvoiceList = (props) => {
    const navigation = useNavigation();
    const {type_invoice} = props 
    const [check, setCheck] = useState(false)
    const [search, setSearch] = useState("");
    const [loading,setLoading] = useState(true)
    const [countPage, setCountPage] = useState(1)
    const [modalPay, setModalPay] = useState(false)
    const [invoice, setInvoice] = useState([])
    const [list, setList] = useState([])
    let [detail,setDetail] = useState(false)
    let [details,setDetails] = useState([])
    let [invoice_id, setInvoice_id] = useState("")

    const callback = (flag,response) =>{
        console.log(response)
        setLoading(false)
        if(flag){
            ToastAndroid.show("Cargando Facturas...",ToastAndroid.SHORT)
            let account_moves=[]
            response.facturas_ids.forEach((element)=>{
                console.log(element.detalles_ids)
            account_moves.push({id: parseInt(element.id),balance: element.saldo,details:element.detalles_ids, date_invoice: element.fecha_factura,value: element.valor, partner: element.partner_id, state: element.estado})
            })
            setInvoice(account_moves)
            setList(accounts_moves)
            
        }else{
        ToastAndroid.show("Estamos teniendo problemas en el servidor, intentalo más tarde",ToastAndroid.SHORT)
        }
        
    };

    const goToHome = () => {
      navigation.navigate('Menu',setCountPage[1] )
    }
    const searchItems = (searchedValue) => {
        let newList = [];
        if (searchedValue === "") {
            setSearch(searchedValue);
            setInvoice(list)
            return
        }else{
            invoice.forEach(element => {    
                if ((element.partner).toLowerCase().includes(searchedValue.toLowerCase())) {
                    newList.push(element);
                }
            });
            setSearch(searchedValue);
            setInvoice(newList);
        }
    }
    const reloadInvoice = () =>{
        setCountPage(countPage+1)
        setInvoice([])
        setList([])
        if(type_invoice==="in_invoice"){
            setCheck(true)
        }else{
            setCheck(false)
        }
        const data={type:type_invoice,pagina:countPage}
        ListInvoice(data,callback)
    }
    const loadInvoice = () =>{
     // setLoading(true)
        setCountPage(1);
        if(type_invoice==="in_invoice"){
            setCheck(true)
        }else{
            setCheck(false)
        }
        const data={type:type_invoice,pagina:countPage}
        ListInvoice(data,callback)
    }

    useEffect(()=>{
        loadInvoice()
    },[type_invoice]) 

    const  goToCreateInvoice = () => {
     navigation.navigate("crearFactura",{})
    }
    return (
      <>
      <Text style={{alignSelf: 'center',fontWeight: 'bold',color:'#0582CA',fontSize:25,marginVertical:10}}>Agregar facturas</Text>
        <View style={{ marginTop: 5 }}>  
          <View style={{ marginHorizontal: 5, marginVertical: 10, }}>
            <View style={{ width: '100%', flexDirection: 'row', backgroundColor: 'transparent', borderRadius: 10, padding: 5, borderWidth: 1, borderColor: '#0A1045' }}>
              <View style={{ width: '35%', justifyContent: 'center', }}>
                <Button
                  onPress={goToHome}
                  containerStyle={{ alignSelf: 'flex-start' }}
                  type="clear"
                  icon={
                  <Icon name="arrow-left"
                    size={30}
                    color='black'
                  />
                  }
                />
              </View>
              <View style={{ width: '50%', justifyContent: 'center' }}>
                <Text style={{color:'black',  fontWeight: 'bold' }}>Facturas</Text>
                <View>
                  <Text style={{color:'black',  }}>
                  <Text style={{ fontWeight: 'bold' }}>#</Text>
                    {invoice.length} 
                  </Text>
                </View>
              </View>
              <View>
                {check &&<Button
                  onPress={goToCreateInvoice}
                  containerStyle={{ alignSelf: 'flex-end' }}
                  type="clear"
                  icon={
                    <Icon name="clean-hands"
                      size={30}
                      color={'black'}
                    />
                  }
                />}
                <Button
                  onPress={() => {reloadInvoice()}}
                  containerStyle={{ alignSelf: 'flex-end' }}
                  type="clear"
                  icon={
                    <Icon name="file-download"
                    size={30}
                    color={'black'}
                    />}
                />
              </View>
            </View>
          </View>    
          {<View style={styles.container}>
            <FlatList
              ListHeaderComponent={
              <View style={{ width: '100%', flexDirection: 'row', backgroundColor: 'transparent', marginTop: 5 }}>
                <View style={{ width: '90%',backgroundColor:'#F9F9F9c0',marginLeft:'5%' }}>
                  <SearchBar
                    styleContainer={{ borderWidth: 1, marginTop: 5 }}
                      icon="search"
                      placeholder='Buscar por cliente...'
                      action={() => { null }}
                      value={search}
                      onChangeText={(value) => searchItems(value)} 
                    /> 
                  <ActivityIndicator
                    animating={loading}
                    color="#003049"
                    size="large"
                    style={{position:'absolute',alignSelf:'center',marginTop:'30%'}}
                  />
                </View>  
              </View>}
              stickyHeaderIndices={[0]}
              data={invoice}
              renderItem={
              ({ item }) =>
                <HistoryItem setFlag={() => setFlag(!flag)} openModalPay={(item) => { setInvoice_id(item.id); setModalPay(true); }} openModalDetail={()=>{setDetail(true); setDetails(item.details)}}  item={item} navigation={navigation} /> 
              }
                keyExtractor={item => "key" + item.id}
                ListFooterComponent={<View style={{ height: 250 }} />}
              />
            </View>}
             </View>
            <Modal
                children={1}
                isVisible={modalPay}
                onRequestClose={() => {
                    setModalPay(false)
                }}
                onBackButtonPress={() => {
                    setModalPay(false)
                }}
                onBackdropPress={() => {
                    setModalPay(false)
                }}
            >
              <ModalRegisterPayment closeModal={()=>{setModalPay(false);setCountPage(1); loadInvoice()}} invoice_id={invoice_id}/>
            </Modal>
            <Modal
                children={""}
                style={{ flex: 1 }}
                isVisible={detail}
                onRequestClose={() => {
                    setDetail(false);
                }}
                onBackButtonPress={() => {
                    setDetail(false);
                }}
                onBackdropPress={() => {
                    setDetail(false);
                }}
            >
                <ModalDetailInvoice details={details} closeModal={()=>{setDetail(false)}} />
              
            </Modal>
        </>
    );
}

var styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        borderWidth: 1,
        margin: 5,
        borderRadius: 10,
        borderColor: '#0A1045'
    },
    header: {
        flexDirection: 'row',
        borderBottomColor: "black",
        borderBottomWidth: 1,
        borderTopLeftRadius: 9,
        borderTopRightRadius: 10,
        padding: 10,
        alignItems: 'center',
        backgroundColor: 'black'
    },
    label: {
        fontSize: 15,
        width: '30%',
        fontWeight: 'bold',
        color: 'white'
    },
    containerHorizontal: {
        flexDirection: "row",
        backgroundColor: 'rgba(7,162,186,0.5)',

    },
    searchBarInput: {
        backgroundColor: 'white',
        color: 'black'
    },
    searchBarContainer: {
        borderRadius: 8,
        marginHorizontal: 5,
        backgroundColor: 'white',
        padding: 0,
    },

});
import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";
export const ItemInvoice = (props) => {
    const { item } = props
    return (
        <TouchableOpacity onPress={() => {}} style={{
            flexDirection: 'row',
            backgroundColor: '#fafafafa', padding: 3, borderBottomWidth: 1, borderBottomColor: 'gray'
        }}>
            <View style={{ borderRightWidth: 1, width: '25%', justifyContent: 'center' }}>
                <Text style={{ fontSize: 13, textAlign: 'center' }}>{item.name}</Text>
            </View>
            <View style={{ borderRightWidth: 1, width: '30%', justifyContent: 'center' }}>
                <Text style={{ fontSize: 13, textAlign: 'center' }}>{item.quantity}</Text>
            </View>
            <View style={{ borderRightWidth: 1, width: '25%', justifyContent: 'center' }}>
                <Text style={{ fontSize: 13, textAlign: 'center' }}>{item.price_unit}</Text>
            </View>
            
        </TouchableOpacity>
    )
}

var styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        borderBottomColor: "#BEBEBE",
        borderBottomWidth: 1,
        marginHorizontal: 10,
        paddingVertical: 2,
        alignItems: 'center',
        justifyContent: 'center',

    },
    label: {
        fontSize: 14
    },
    titleHeader: {
        marginHorizontal: 12,
        fontSize: 14,
        fontWeight: "bold",
        marginTop: 4,
        marginBottom: 5
    },
    input: {
        fontSize: 20,
    },
});

import  React, { useEffect, useState } from 'react';
import { SearchPicker } from '../../../utils/picker/picker';
import { Input, Icon } from 'react-native-elements';
import {Alert, Switch,View, StyleSheet, ImageBackground,Text,  TouchableOpacity, FlatList, SafeAreaView, ToastAndroid } from 'react-native';
import { ReturnData, } from '../../../services/services';
import { useNavigation} from '@react-navigation/native'
import { CreateInvoiceSupplier } from '../../../services/box.services';


export const InvoiceSupplier = (props) => {
    const navigation = useNavigation();
    const [products, setProducts] = useState([]);
    let [suppliers, setSuppliers] = useState([])
    let [partner,setPartner] = useState('')
    let [itemSelected, setItemSelected] = useState('');
    let [selectedProduct, setSelectedProduct]  = useState([]);

    const SendInvoice = () =>{
        let detalles=[]
        selectedProduct.forEach((element)=>{
            detalles.push({product_id:parseInt(element.value), price_unit: parseInt(element.price), name:element.label,product_uom_qty: element.amount, aplicaimpuesto: element.aplicaImpuesto })
        })
        const jsonenvio = {
            partner_id:parseInt(partner.value),
            detalles: detalles
        }
        console.log("JSON ENVIO", jsonenvio)
        CreateInvoiceSupplier(jsonenvio,(flag,response)=>{
            if (flag){
                if(response.errores){
                    if(response.errores.error){
                        Alert.alert('¡Atención! ',response.errores.error);
                    }
                }else{
                    ToastAndroid.show("Factura creada correctamente", ToastAndroid.SHORT);
                    navigation.navigate('Menu'); 
                }
            }
        })
        setSelectedProduct([])
        setItemSelected('')
        setPartner('')
    }
    
    useEffect(()=>{
        const product = [];
        const data={}
         ReturnData(data,(flag,response) =>{
            if(flag){
                response.product.forEach((element)=>{
                    if(element.compra===true){
                        product.push({label: element.name, value: element.id, price:0, amount: 0, aplicaImpuesto: false})
                    }
                })
                product.sort((a,b) =>{
                    return (a.label < b.label) ? -1 : 1
                })
                setProducts(product)
                let partner = []
                response.res_partner.forEach((element)=>{
                    if(element.supplier_rank >0){
                        partner.push({label: element.name, value: element.id})
                    }
                    setSuppliers(partner)   
                })
            }
        })
    },[])

    const goToFacturaproverdor = () =>{
        navigation.navigate('Facturas',{})

    }
    return (
        <View style={{backgroundColor:'#ffff',height:'100%'}}>
        <View style={{flexDirection:'row',marginTop:'5%'}}>
           <TouchableOpacity onPress={goToFacturaproverdor}>
                <Icon name='arrow-back' color={'black'} size={40} style={{marginLeft:10,marginTop:8}}/>
            </TouchableOpacity>  
            <Text style={{alignSelf: 'center',fontWeight: 'bold',color:'#0582CA',fontSize:25,marginVertical:10}}>Crear Factura Proveedor</Text>
        </View>
            <View style={style.body}>
                <Text style={style.text}>Proveedor</Text>
                <SearchPicker
                    style={{ button: { marginHorizontal: 0 } }}
                    value={partner}
                    items={suppliers}
                    onChange={(itemValue) => {
                    setPartner(itemValue)}}
                />
                <Text style={style.text}>Productos</Text>
                <SearchPicker
                    style={{ button: { marginTop: 10 } }}
                    value={itemSelected}
                    items={products}
                    onChange={(product) => { selectedProduct.push(product);
                    setItemSelected(product);
                    }}
                />
                <SafeAreaView style={{height:350, marginTop:10, backgroundColor: 'rgba(255,255,255,0.2)'}}>
                    <FlatList style={{marginTop:20}}
                        data={selectedProduct}
                        scrollEnabled={true}
                        renderItem={({ item, key }) =>
                            <ItemQuotation item={item} key={key} />} 
                    />
                </SafeAreaView>
            </View>
            <View style={style.buttonRU}>
                <TouchableOpacity onPress={SendInvoice}>
                    <Text style={style.button}>Crear Factura</Text>
                </TouchableOpacity> 
            </View>
        </View>
    )
}
export default InvoiceSupplier;
  
    const style = StyleSheet.create({
      container: {  
          alignSelf:'flex-start',
           height: '100%',
           width:'100%', 
          },  
      body:{
        alignSelf:'center',
        flexDirection:'column',
        paddingTop:70,
        marginHorizontal: 10,
        width: '80%',
      },
      profile:{
        flexDirection:'row',
        alignSelf:'center',
        width: '50%',
    },
      person:{
        padding: 7,
        width: '86%',
        fontSize: 17,
        fontFamily:'sans-serif-light',   
    },
      text:{
        textAlign:'center',
        color:'black',
        paddingTop:10,
        paddingBottom:10
      },
      buttonRU:{
        flexDirection:'row',
        backgroundColor: 'rgba(0,0,0,0.2)',
        alignSelf: 'center',
        marginBottom:50,
        marginLeft: 8,
        width: '80%',
        height:'5%',
        borderRadius:4,
        justifyContent: 'center'
    },
      button:{ 
        fontSize: 20, 
        color:'#fff', 
        alignSelf:'flex-end',
        fontWeight: 'bold' 
      },
      
    })

const ItemQuotation =(props) => {
    const {item} =  props
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = (value) => {
        item.aplicaImpuesto= value
        setIsEnabled(previousState => !previousState);
    };
    return (
        <View style={{flexDirection: 'row', padding: 3, borderBottomWidth: 1, borderBottomColor: 'gray'}}>
            <View style={{ borderRightWidth: 1, width: '33%', }}>
                <Text style={{ fontSize: 15, textAlign: 'center' }}>{item.label}</Text>
            </View>
            <View style={{ borderRightWidth: 1, width: '33%', }}>
                <Input style = {style.person}
                    value={item.price} 
                    onChangeText = {(text) =>{item.price= text}}
                    placeholder="$ Unidad" 
                    keyboardType='numeric'
                    placeholderTextColor={'rgba(255,255,255,0.5)'}      
                /> 
            </View> 
            <View style={{ borderRightWidth: 1, width: '33%', }}>
                <Input style = {style.person}
                       value={item.amount} 
                       onChangeText = {(text) =>{item.amount=text}}
                       placeholder="Cantidad" 
                       keyboardType='numeric'
                       placeholderTextColor={'rgba(255,255,255,0.5)'}      
                /> 
                <Text style={{ fontSize: 15, textAlign: 'center' }}>¿Impuesto?</Text>
                <Switch
                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                    thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                    ios_backgroundColor="#3e3e3e"
                    onValueChange={toggleSwitch}
                    value={isEnabled}
                />
            </View> 
        </View>
    )
}
    
        

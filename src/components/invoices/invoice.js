import  React,{useState, useEffect} from 'react';
import { View, StyleSheet, ImageBackground} from 'react-native';
import { InvoiceList } from './components/invoiceList';
const Invoices = (props) => {
  console.log("props Invoices",props)
    const {navigation} = props
    const{type_invoice} = props.route.params 

return (
    <View style={{ backgroundColor:'#ffff',height:'100%'}}>
      <View style={{marginTop: 30, margin: 15, borderRadius: 10, }}>        
        <InvoiceList navigation= {navigation} type_invoice={type_invoice}/>
      </View>       
    </View>
  )
}

export default Invoices
const style = StyleSheet.create({
    
})